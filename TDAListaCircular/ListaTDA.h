#ifndef LISTATDA_H_INCLUDED
#define LISTATDA_H_INCLUDED
// Lista Circular
typedef struct Nodo_Circular {
int valor;

struct Nodo_Circular *sig;
} Nodo_Circular;

typedef struct Lista_Circular
{
Nodo_Circular *Nuevo_Circular;
} Lista_Circular;

void Crear(Lista_Circular*);

void Destruir(Lista_Circular*);

void AltaAlPrincipio(Lista_Circular*, int);

void AltaAlFinal(Lista_Circular*, int);

void BajaAlPrincipio(Lista_Circular*);

void BajaAlFinal(Lista_Circular*);

void Recorrer(Lista_Circular);

void BuscarUnElemento(Lista_Circular, int);

int CantidadDeElementos(Lista_Circular);

void MostrarMenu();

Nodo_Circular BuscarUltimoNodo(Lista_Circular*);

#endif // LISTATDA_H_INCLUDED
