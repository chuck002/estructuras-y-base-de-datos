#include <stdio.h>
#include <stdlib.h>

#include "ListaTDA.h"

void Crear(Lista_Circular *l)
{
l->Nuevo_Circular = malloc(sizeof(Nodo_Circular));
l->Nuevo_Circular = NULL;
}

void Destruir(Lista_Circular *l)
{
void Destruir(Lista_Circular *l)
{
//free (l->Nuevo_Circular);
}

void AltaAlPrincipio(Lista_Circular *l, int valor)
{
	Nodo_Circular *n;
	n = malloc(sizeof(Nodo_Circular));
	n->valor = valor;
	n->sig = l->Nuevo_Circular;
	
	l->Nuevo_Circular = n;
//free (l->Nuevo_Circular);
}

void AltaAlFinal(Lista_Circular *l, int valor)
{
	Nodo_Circular *n;
	n = malloc(sizeof(Nodo_Circular));
	n->valor = valor;
	n->sig = l->Nuevo_Circular;
	if(l->Nuevo_Circular == NULL)
	{
		l->Nuevo_Circular = n;
	}
	else
	{
		Nodo_Circular *ultimo = BuscarUltimoNodo(l);
		ultimo->sig = n;
	}
}

void BajaAlPrincipio(Lista_Circular *l)
{
	if(l->Nuevo_Circular == NULL)
	{
		printf("\n\n***No existe nada para borrar. ***\n\nAdios");
	}
	else{
		Nodo_Circular *tmp = l->Nuevo_Circular;
		l->Nuevo_Circular = tmp->sig;
		free(tmp);
	}
	
}


void BajaAlFinal(Lista_Circular *l)
{
	if(l->Nuevo_Circular == NULL)
	{
		printf("\n\n***No existe nada para borrar. ***\n\nAdios");
	}
	else{
		Nodo_Circular *ultimo = BuscarUltimoNodo(l);
		Nodo_Circular *tmp = l->Nuevo_Circular;
		while(tmp->sig != ultimo)
			{
				tmp = tmp->sig;
			}
		tmp->sig = l->Nuevo_Circular;
		free(ultimo);
	}
}
void Destruir(Lista_Circular *l)
{
//free (l->Nuevo_Circular);
}

void AltaAlPrincipio(Lista_Circular *l, int valor)
{
	n = malloc(sizeof(Nodo_Circular));
	n->valor = valor;
	n->sig = l->Nuevo_Circular;
	
	l->Nuevo_Circular = n;

void Recorrer(Lista_Circular l)
{
	if(l->Nuevo_Circular == NULL)
	{
		printf("\n\nNo hay datos para mostrar.");
	}else{
	Nodo_Circular tmp = l->Nuevo_Circular;
	int cont = 1;
	while(tmp->sig != l->Nuevo_Circular)
	{
		printf"\n\nDato nº %d: %d",cont, tmp->valor);
		cont++;
		tmp = tmp->sig;
	}
	}
}

void BuscarUnElemento(Lista_Circular l, int valor)
{
	bool encontrado = false;
	if(l->Nuevo_Circular == NULL)
	{
		printf("\n\nNo hay datos para mostrar.");
	}else{
		Nodo_Circular *tmp = l->Nuevo_Circular;
		while(valor != tmp->valor && tmp->sig != l->Nuevo_Circular)
		{
			if(tmp == tmp->valor)
			{
				encontrado = true;
			}
			tmp = tmp->sig;
		}
		if(encontrado)
		{
			printf("\n\nValor encontrado!!!");
		}else{
			printf("\n\nValor NO encontrado. No se encuentra dentro de la lista.");
		}
	}
}

int CantidadDeElementos(Lista_Circular l)
{
	int elementos = 0;
	if(l->Nuevo_Circular == NULL)
	{
		return elementos;
	}
	else{
		Nodo_Circular *tmp = l->Nuevo_Circular;
		while(tmp->sig != l->Nuevo_Circular)
		{
			elementos++;
			tmp = tmp->sig;
		}
		return elementos;
	}

}

void MostrarMenu()
{
	printf("\nBienvenido al programa de listas Circulares.");
	printf("\nPor favor elija una opcion. ");
	printf("\n\n1. Crear Lista.");
	printf("\n2. Borrar Lista.");
	printf("\n3. Agregar dato al principio.");
	printf("\n4. Agregar dato al final.");
	printf("\n5. Borrar dato del principio.");
	printf("\n6. Borrar dato del final.");
	printf("\n7. Mostrar todos los elementos.");
	printf("\n8. Buscar un elemento.");
	printf("\n9. Mostrar la cantidad de elementos de la lista.");
	printf("\n\nElige una opcion: ");
}

Nodo_Circular* BuscarUltimoNodo(Lista_Circular l)
{
	Nodo_Circular *nodo = l->Nuevo_Circular;
	while(nodo->sig != l->Nuevo_Circular)
	{
		nodo = nodo->sig;
	}
	return nodo;
}