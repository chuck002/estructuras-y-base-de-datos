##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=TDAListaCircular
ConfigurationName      :=Debug
WorkspaceConfiguration := $(ConfigurationName)
WorkspacePath          :=/home/javy/Documentos/ProyectosEnC
ProjectPath            :=/home/javy/Documentos/ProyectosEnC/TDAListaCircular
IntermediateDirectory  :=../build-$(ConfigurationName)/TDAListaCircular
OutDir                 :=../build-$(ConfigurationName)/TDAListaCircular
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Javier Luna
Date                   :=02/05/20
CodeLitePath           :=/home/javy/.codelite
LinkerName             :=/usr/bin/g++-7
SharedObjectLinkerName :=/usr/bin/g++-7 -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../build-$(ConfigurationName)/bin/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :=$(IntermediateDirectory)/ObjectsList.txt
PCHCompileFlags        :=
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++-7
CC       := /usr/bin/gcc-7
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=../build-$(ConfigurationName)/TDAListaCircular/ListaTDA.c$(ObjectSuffix) ../build-$(ConfigurationName)/TDAListaCircular/main.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: MakeIntermediateDirs $(OutputFile)

$(OutputFile): ../build-$(ConfigurationName)/TDAListaCircular/.d $(Objects) 
	@mkdir -p "../build-$(ConfigurationName)/TDAListaCircular"
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@mkdir -p "../build-$(ConfigurationName)/TDAListaCircular"
	@mkdir -p ""../build-$(ConfigurationName)/bin""

../build-$(ConfigurationName)/TDAListaCircular/.d:
	@mkdir -p "../build-$(ConfigurationName)/TDAListaCircular"

PreBuild:


##
## Objects
##
../build-$(ConfigurationName)/TDAListaCircular/ListaTDA.c$(ObjectSuffix): ListaTDA.c ../build-$(ConfigurationName)/TDAListaCircular/ListaTDA.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/TDAListaCircular/ListaTDA.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/ListaTDA.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/TDAListaCircular/ListaTDA.c$(DependSuffix): ListaTDA.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/TDAListaCircular/ListaTDA.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/TDAListaCircular/ListaTDA.c$(DependSuffix) -MM ListaTDA.c

../build-$(ConfigurationName)/TDAListaCircular/ListaTDA.c$(PreprocessSuffix): ListaTDA.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/TDAListaCircular/ListaTDA.c$(PreprocessSuffix) ListaTDA.c

../build-$(ConfigurationName)/TDAListaCircular/main.c$(ObjectSuffix): main.c ../build-$(ConfigurationName)/TDAListaCircular/main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/TDAListaCircular/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/TDAListaCircular/main.c$(DependSuffix): main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/TDAListaCircular/main.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/TDAListaCircular/main.c$(DependSuffix) -MM main.c

../build-$(ConfigurationName)/TDAListaCircular/main.c$(PreprocessSuffix): main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/TDAListaCircular/main.c$(PreprocessSuffix) main.c


-include ../build-$(ConfigurationName)/TDAListaCircular//*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(IntermediateDirectory)


