##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=TDAVector
ConfigurationName      :=Debug
WorkspaceConfiguration := $(ConfigurationName)
WorkspacePath          :=/home/javy/Documentos/ProyectosEnC
ProjectPath            :=/home/javy/Documentos/ProyectosEnC/TDAVector
IntermediateDirectory  :=../build-$(ConfigurationName)/TDAVector
OutDir                 :=../build-$(ConfigurationName)/TDAVector
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Javier Luna
Date                   :=02/05/20
CodeLitePath           :=/home/javy/.codelite
LinkerName             :=/usr/bin/g++-7
SharedObjectLinkerName :=/usr/bin/g++-7 -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../build-$(ConfigurationName)/bin/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :=$(IntermediateDirectory)/ObjectsList.txt
PCHCompileFlags        :=
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++-7
CC       := /usr/bin/gcc-7
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=../build-$(ConfigurationName)/TDAVector/up_up_up_ProyectosC_TDAVector_main.c$(ObjectSuffix) ../build-$(ConfigurationName)/TDAVector/up_up_up_ProyectosC_TDAVector_Vector.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: MakeIntermediateDirs $(OutputFile)

$(OutputFile): ../build-$(ConfigurationName)/TDAVector/.d $(Objects) 
	@mkdir -p "../build-$(ConfigurationName)/TDAVector"
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@mkdir -p "../build-$(ConfigurationName)/TDAVector"
	@mkdir -p ""../build-$(ConfigurationName)/bin""

../build-$(ConfigurationName)/TDAVector/.d:
	@mkdir -p "../build-$(ConfigurationName)/TDAVector"

PreBuild:


##
## Objects
##
../build-$(ConfigurationName)/TDAVector/up_up_up_ProyectosC_TDAVector_main.c$(ObjectSuffix): ../../../ProyectosC/TDAVector/main.c ../build-$(ConfigurationName)/TDAVector/up_up_up_ProyectosC_TDAVector_main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/ProyectosC/TDAVector/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_ProyectosC_TDAVector_main.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/TDAVector/up_up_up_ProyectosC_TDAVector_main.c$(DependSuffix): ../../../ProyectosC/TDAVector/main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/TDAVector/up_up_up_ProyectosC_TDAVector_main.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/TDAVector/up_up_up_ProyectosC_TDAVector_main.c$(DependSuffix) -MM ../../../ProyectosC/TDAVector/main.c

../build-$(ConfigurationName)/TDAVector/up_up_up_ProyectosC_TDAVector_main.c$(PreprocessSuffix): ../../../ProyectosC/TDAVector/main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/TDAVector/up_up_up_ProyectosC_TDAVector_main.c$(PreprocessSuffix) ../../../ProyectosC/TDAVector/main.c

../build-$(ConfigurationName)/TDAVector/up_up_up_ProyectosC_TDAVector_Vector.c$(ObjectSuffix): ../../../ProyectosC/TDAVector/Vector.c ../build-$(ConfigurationName)/TDAVector/up_up_up_ProyectosC_TDAVector_Vector.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/ProyectosC/TDAVector/Vector.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_ProyectosC_TDAVector_Vector.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/TDAVector/up_up_up_ProyectosC_TDAVector_Vector.c$(DependSuffix): ../../../ProyectosC/TDAVector/Vector.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/TDAVector/up_up_up_ProyectosC_TDAVector_Vector.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/TDAVector/up_up_up_ProyectosC_TDAVector_Vector.c$(DependSuffix) -MM ../../../ProyectosC/TDAVector/Vector.c

../build-$(ConfigurationName)/TDAVector/up_up_up_ProyectosC_TDAVector_Vector.c$(PreprocessSuffix): ../../../ProyectosC/TDAVector/Vector.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/TDAVector/up_up_up_ProyectosC_TDAVector_Vector.c$(PreprocessSuffix) ../../../ProyectosC/TDAVector/Vector.c


-include ../build-$(ConfigurationName)/TDAVector//*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(IntermediateDirectory)


