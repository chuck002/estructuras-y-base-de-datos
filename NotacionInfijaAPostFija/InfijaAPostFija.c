#include <stdio.h>
#include <stdlib.h>

#include "InfijaAPostFija.h"


void Crear(pila *p)
{
	p->p = NULL;
}

void Destruir(pila *p)
{
	if(p->p == NULL)
	{
		printf("\nNo hay datos en la pila.");
	}else
	{
		nodo * borra = p->p;
		do
		{
			borra = p->p;
			p->p = p->p->sig;
			free(borra);
		}while(p->p != NULL);
	}
}

void Apilar(pila * p, char valor)
{
	nodo *nuevo = (nodo*) malloc(sizeof(nodo));
	nuevo->valor = valor;
	if(p->p == NULL)
	{
		nuevo->sig = NULL;
		p->p = nuevo;
	}else
	{
		nuevo->sig = p->p;
		p->p = nuevo;
	}
}

void Desapilar(pila *p)
{
	nodo *dato;
	if(p->p == NULL)
	{
		printf("\nNo hay nada para desapilar.");
	}else{
		dato = p->p;
		p->p = p->p->sig;
		free(dato);
	}
}
int Tope(pila p)
{
	if(p.p == NULL)
	{
		printf("\nNo hay nada para mostrar.");
		return 0;
	}else{
		return p.p->valor;
	}
}

int Vacia(pila p)
{
	if(p.p == NULL)
	{
		return 1; // 1 si esta vacia.
	}else
	{
		return 0; // 0 si esta llena.
	}
}

int ComprobarPrecedencia(pila *p, char valor)
{
	switch(valor)
				{
					case '+':
					return 1;
					break;
					case '-':
					return 1;
					break;
					case '*':
					return 2;
					break;
					case '/':
					return 2
					break;
					default:
					return 0;
					break;
				}
}
