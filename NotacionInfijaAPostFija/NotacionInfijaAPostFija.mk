##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=NotacionInfijaAPostFija
ConfigurationName      :=Debug
WorkspaceConfiguration := $(ConfigurationName)
WorkspacePath          :=/home/javy/Documentos/ProyectosEnC
ProjectPath            :=/home/javy/Documentos/ProyectosEnC/NotacionInfijaAPostFija
IntermediateDirectory  :=../build-$(ConfigurationName)/NotacionInfijaAPostFija
OutDir                 :=../build-$(ConfigurationName)/NotacionInfijaAPostFija
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Javier Luna
Date                   :=11/05/20
CodeLitePath           :=/home/javy/.codelite
LinkerName             :=/usr/bin/g++-7
SharedObjectLinkerName :=/usr/bin/g++-7 -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../build-$(ConfigurationName)/bin/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :=$(IntermediateDirectory)/ObjectsList.txt
PCHCompileFlags        :=
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++-7
CC       := /usr/bin/gcc-7
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=../build-$(ConfigurationName)/NotacionInfijaAPostFija/main.c$(ObjectSuffix) ../build-$(ConfigurationName)/NotacionInfijaAPostFija/InfijaAPostFija.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: MakeIntermediateDirs $(OutputFile)

$(OutputFile): ../build-$(ConfigurationName)/NotacionInfijaAPostFija/.d $(Objects) 
	@mkdir -p "../build-$(ConfigurationName)/NotacionInfijaAPostFija"
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@mkdir -p "../build-$(ConfigurationName)/NotacionInfijaAPostFija"
	@mkdir -p ""../build-$(ConfigurationName)/bin""

../build-$(ConfigurationName)/NotacionInfijaAPostFija/.d:
	@mkdir -p "../build-$(ConfigurationName)/NotacionInfijaAPostFija"

PreBuild:


##
## Objects
##
../build-$(ConfigurationName)/NotacionInfijaAPostFija/main.c$(ObjectSuffix): main.c ../build-$(ConfigurationName)/NotacionInfijaAPostFija/main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/NotacionInfijaAPostFija/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/NotacionInfijaAPostFija/main.c$(DependSuffix): main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/NotacionInfijaAPostFija/main.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/NotacionInfijaAPostFija/main.c$(DependSuffix) -MM main.c

../build-$(ConfigurationName)/NotacionInfijaAPostFija/main.c$(PreprocessSuffix): main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/NotacionInfijaAPostFija/main.c$(PreprocessSuffix) main.c

../build-$(ConfigurationName)/NotacionInfijaAPostFija/InfijaAPostFija.c$(ObjectSuffix): InfijaAPostFija.c ../build-$(ConfigurationName)/NotacionInfijaAPostFija/InfijaAPostFija.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/NotacionInfijaAPostFija/InfijaAPostFija.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/InfijaAPostFija.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/NotacionInfijaAPostFija/InfijaAPostFija.c$(DependSuffix): InfijaAPostFija.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/NotacionInfijaAPostFija/InfijaAPostFija.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/NotacionInfijaAPostFija/InfijaAPostFija.c$(DependSuffix) -MM InfijaAPostFija.c

../build-$(ConfigurationName)/NotacionInfijaAPostFija/InfijaAPostFija.c$(PreprocessSuffix): InfijaAPostFija.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/NotacionInfijaAPostFija/InfijaAPostFija.c$(PreprocessSuffix) InfijaAPostFija.c


-include ../build-$(ConfigurationName)/NotacionInfijaAPostFija//*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(IntermediateDirectory)


