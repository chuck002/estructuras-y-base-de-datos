#ifndef INFIJAAPOSTFIJA_H_INCLUDED
#define INFIJAAPOSTFIJA_H_INCLUDED

typedef struct Nodo{
	char valor;
	struct Nodo *sig;
}nodo;

typedef struct Pila
{
	nodo *p;
}pila;

void Crear(pila*);

void Destruir(pila*);

void Apilar(pila*, char);

void Desapilar(pila*);

int Tope(pila);

int Vacia(pila);

void ComprobarPrecedencia(pila *, char);

#endif // INFIJAAPOSTFIJA_H_INCLUDED