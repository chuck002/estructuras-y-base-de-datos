#ifndef TDACOLAVECTORCIRCULAR_H_INCLUDED
#define TDACOLAVECTORCIRCULAR_H_INCLUDED

typedef struct Vector{
	int pos_primero;
	int pos_ultimo;
	int vector[6];
}cola;

void Crear(cola*);

void Destruir(cola*);

void Encolar(cola*, int);

void Desencolar(cola*);

int Tope(cola);

int Fondo(cola);

int Vacia(cola);

#endif