##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=TDAColaVectorCircular
ConfigurationName      :=Debug
WorkspaceConfiguration := $(ConfigurationName)
WorkspacePath          :=/home/javy/Documentos/ProyectosEnC
ProjectPath            :=/home/javy/Documentos/ProyectosEnC/TDAColaVectorCircular
IntermediateDirectory  :=../build-$(ConfigurationName)/TDAColaVectorCircular
OutDir                 :=../build-$(ConfigurationName)/TDAColaVectorCircular
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Javier Luna
Date                   :=10/05/20
CodeLitePath           :=/home/javy/.codelite
LinkerName             :=/usr/bin/g++-7
SharedObjectLinkerName :=/usr/bin/g++-7 -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../build-$(ConfigurationName)/bin/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :=$(IntermediateDirectory)/ObjectsList.txt
PCHCompileFlags        :=
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++-7
CC       := /usr/bin/gcc-7
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=../build-$(ConfigurationName)/TDAColaVectorCircular/TDAColaVectorCircular.c$(ObjectSuffix) ../build-$(ConfigurationName)/TDAColaVectorCircular/main.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: MakeIntermediateDirs $(OutputFile)

$(OutputFile): ../build-$(ConfigurationName)/TDAColaVectorCircular/.d $(Objects) 
	@mkdir -p "../build-$(ConfigurationName)/TDAColaVectorCircular"
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@mkdir -p "../build-$(ConfigurationName)/TDAColaVectorCircular"
	@mkdir -p ""../build-$(ConfigurationName)/bin""

../build-$(ConfigurationName)/TDAColaVectorCircular/.d:
	@mkdir -p "../build-$(ConfigurationName)/TDAColaVectorCircular"

PreBuild:


##
## Objects
##
../build-$(ConfigurationName)/TDAColaVectorCircular/TDAColaVectorCircular.c$(ObjectSuffix): TDAColaVectorCircular.c ../build-$(ConfigurationName)/TDAColaVectorCircular/TDAColaVectorCircular.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/TDAColaVectorCircular/TDAColaVectorCircular.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/TDAColaVectorCircular.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/TDAColaVectorCircular/TDAColaVectorCircular.c$(DependSuffix): TDAColaVectorCircular.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/TDAColaVectorCircular/TDAColaVectorCircular.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/TDAColaVectorCircular/TDAColaVectorCircular.c$(DependSuffix) -MM TDAColaVectorCircular.c

../build-$(ConfigurationName)/TDAColaVectorCircular/TDAColaVectorCircular.c$(PreprocessSuffix): TDAColaVectorCircular.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/TDAColaVectorCircular/TDAColaVectorCircular.c$(PreprocessSuffix) TDAColaVectorCircular.c

../build-$(ConfigurationName)/TDAColaVectorCircular/main.c$(ObjectSuffix): main.c ../build-$(ConfigurationName)/TDAColaVectorCircular/main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/TDAColaVectorCircular/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/TDAColaVectorCircular/main.c$(DependSuffix): main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/TDAColaVectorCircular/main.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/TDAColaVectorCircular/main.c$(DependSuffix) -MM main.c

../build-$(ConfigurationName)/TDAColaVectorCircular/main.c$(PreprocessSuffix): main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/TDAColaVectorCircular/main.c$(PreprocessSuffix) main.c


-include ../build-$(ConfigurationName)/TDAColaVectorCircular//*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(IntermediateDirectory)


