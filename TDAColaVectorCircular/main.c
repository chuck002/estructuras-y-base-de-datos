#include <stdio.h>

#include "TDAColaVectorCircular.h"

int main(int argc, char **argv)
{
	cola c;
	Crear(&c);
	Encolar(&c, 2);
	Encolar(&c, 7);
	Encolar(&c, 10);
	Encolar(&c, 20);
	Encolar(&c, 3);
	Encolar(&c, 4);
	//Encolar(&c, 5);
	Desencolar(&c);
	Encolar(&c, 5);
	do
	{
			printf("* %i - %i *", Tope(c), Fondo(c));
			Desencolar(&c);
	}while(c.pos_primero != c.pos_ultimo);
	
	Destruir(&c);
}

