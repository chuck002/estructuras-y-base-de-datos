#include <stdio.h>
#include <stdlib.h>

#include "TDAColaVectorCircular.h"

#define TAMANIO 6

void Crear(cola *c)
{
	c->pos_primero = -1;
	c->pos_ultimo = -1;
	printf("\nCrear");
}

void Destruir(cola* c)
{
	for(int i = 0; i < TAMANIO ; i++)
	{
		c->vector[i] = 0;
	}
	c->pos_primero = -1;
	c->pos_ultimo = -1;
	printf("\nDestruir.");
}

void Encolar(cola *c, int valor)
{
	if(c->pos_primero == -1)
	{
		c->pos_primero = 0;
		c->pos_ultimo = 0;
		c->vector[c->pos_primero] = valor;
		c->vector[c->pos_ultimo] = valor;
		printf("\nEncolo primer elemento.");
	}else
	{
		if(c->pos_ultimo < TAMANIO-1)
		{
			c->pos_ultimo++;
			c->vector[c->pos_ultimo] = valor;
		}
		else
		{
			if((c->pos_ultimo - c->pos_primero)+1 == TAMANIO)
			{
				printf("\nEl Vector se encuentra completo. Debe esperar a que se vacie.");
			}
			else
			{
				c->pos_ultimo = 0;
				c->vector[c->pos_ultimo] = valor;
			}
		}
	}
}

void Desencolar(cola* c)
{
	if(c->pos_primero == -1)
	{
		printf("\nNo hay datos para retirar de la cola.");
	}
	else
	{
		if(c->pos_primero != c->pos_ultimo && c->pos_primero != TAMANIO-1)
		{
			c->pos_primero++;
		}else if(c->pos_primero == c->pos_ultimo)
		{
			c->pos_primero = 0;
		}else{
			c->pos_primero = -1;
			c->pos_ultimo = -1;
		}
	}
}

int Tope(cola c)
{
	if(c.pos_primero == -1)
	{
		printf("\nNo hay datos cargados en la cola.");
		return 0;
	}
	else
	{
		return c.vector[c.pos_primero];
	}
}

int Fondo(cola c)
{
	if(c.pos_primero == -1)
	{
		printf("\nNo hay datos cargados en la cola.");
		return 0;
	}
	else
	{
		return c.vector[c.pos_ultimo];
	}
}

int Vacia(cola c)
{
	if(c.pos_primero == -1)
	{
		return 1; // Retorna 1 si esta vacia.
	}else
	{
		return 0; // Retronea 0 si esta llena.
	}
}