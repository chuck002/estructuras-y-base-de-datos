#include <stdlib.h>
#include <stdio.h>

#include "TDAColaLista.h"

nodo *ultimo;

void Crear(cola *c)
{
	c->p = NULL;
	ultimo = NULL;
}

void Destruir(cola *c)
{
	nodo *borra;
	if(c->p == NULL)
	{
		printf("\nNo hay nada para destruir.");
	}else
	{
		do
		{
		borra = c->p;
		c->p = c->p->sig;
		free(borra);
		}while(c->p != NULL);
	}
}

void Encolar(cola *c, int valor)
{
	nodo *nuevo = (nodo*)malloc(sizeof(nodo));
	nuevo->sig = NULL;
	nuevo->valor = valor;
	if(c->p == NULL)
	{
		c->p = nuevo;
		ultimo = nuevo;
	}else
	{
		ultimo->sig = nuevo;
		ultimo = nuevo;
	}
}

void Desencolar(cola *c)
{
	nodo *borra;
	if(c->p == NULL)
	{
		printf("\nNo hay elementos para desdencolar.");
	}else
	{
		borra = c->p;
		c->p = c->p->sig;
		free(borra);
	}
}

int Tope(cola c)
{
	if(c.p == NULL)
	{
		printf("\nNo hay datos para mostrar.");
		return 0;
	}else
	{
		return c.p->valor;
	}
}

int Fondo(cola c)
{
	if(c.p == NULL)
	{
		printf("\nNo hay datos para mostrar.");
		return 0;
	}else
	{
		return ultimo->valor;
	}
}

int Vacia(cola c)
{
	if(c.p == NULL)
	{
		return 1;
	}else
	{
		return 0;
	}
}