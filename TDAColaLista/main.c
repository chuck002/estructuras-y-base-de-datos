#include <stdio.h>

#include "TDAColaLista.h"

int main(int argc, char **argv)
{
	cola c;
	Crear(&c);
	Encolar(&c, 2);
	Encolar(&c, 7);
	Encolar(&c, 10);
	Encolar(&c, 20);
	Encolar(&c, 3);
	while(c.p != NULL)
	{
			printf("* %i - %i *", Tope(c), Fondo(c));
			Desencolar(&c);
	}
	Destruir(&c);
}
