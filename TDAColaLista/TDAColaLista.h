#ifndef LISTATDACOLALISTA_H_INCLUDED
#define LISTATDACOLALISTA_H_INCLUDED

typedef struct Nodo{
	int valor;
	struct Nodo *sig;
}nodo;

typedef struct Cola
{
	nodo *p;
}cola;

void Crear(cola*);

void Destruir(cola*);

void Encolar(cola*, int);

void Desencolar(cola*);

int Tope(cola);

int Fondo(cola);

int Vacia(cola);

#endif // LISTATDACOLALISTA_H_INCLUDED