##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=TDAColaLista
ConfigurationName      :=Debug
WorkspaceConfiguration := $(ConfigurationName)
WorkspacePath          :=/home/javy/Documentos/ProyectosEnC
ProjectPath            :=/home/javy/Documentos/ProyectosEnC/TDAColaLista
IntermediateDirectory  :=../build-$(ConfigurationName)/TDAColaLista
OutDir                 :=../build-$(ConfigurationName)/TDAColaLista
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Javier Luna
Date                   :=04/05/20
CodeLitePath           :=/home/javy/.codelite
LinkerName             :=/usr/bin/g++-7
SharedObjectLinkerName :=/usr/bin/g++-7 -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../build-$(ConfigurationName)/bin/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :=$(IntermediateDirectory)/ObjectsList.txt
PCHCompileFlags        :=
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++-7
CC       := /usr/bin/gcc-7
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=../build-$(ConfigurationName)/TDAColaLista/main.c$(ObjectSuffix) ../build-$(ConfigurationName)/TDAColaLista/TDAColaLista.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: MakeIntermediateDirs $(OutputFile)

$(OutputFile): ../build-$(ConfigurationName)/TDAColaLista/.d $(Objects) 
	@mkdir -p "../build-$(ConfigurationName)/TDAColaLista"
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@mkdir -p "../build-$(ConfigurationName)/TDAColaLista"
	@mkdir -p ""../build-$(ConfigurationName)/bin""

../build-$(ConfigurationName)/TDAColaLista/.d:
	@mkdir -p "../build-$(ConfigurationName)/TDAColaLista"

PreBuild:


##
## Objects
##
../build-$(ConfigurationName)/TDAColaLista/main.c$(ObjectSuffix): main.c ../build-$(ConfigurationName)/TDAColaLista/main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/TDAColaLista/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/TDAColaLista/main.c$(DependSuffix): main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/TDAColaLista/main.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/TDAColaLista/main.c$(DependSuffix) -MM main.c

../build-$(ConfigurationName)/TDAColaLista/main.c$(PreprocessSuffix): main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/TDAColaLista/main.c$(PreprocessSuffix) main.c

../build-$(ConfigurationName)/TDAColaLista/TDAColaLista.c$(ObjectSuffix): TDAColaLista.c ../build-$(ConfigurationName)/TDAColaLista/TDAColaLista.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/TDAColaLista/TDAColaLista.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/TDAColaLista.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/TDAColaLista/TDAColaLista.c$(DependSuffix): TDAColaLista.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/TDAColaLista/TDAColaLista.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/TDAColaLista/TDAColaLista.c$(DependSuffix) -MM TDAColaLista.c

../build-$(ConfigurationName)/TDAColaLista/TDAColaLista.c$(PreprocessSuffix): TDAColaLista.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/TDAColaLista/TDAColaLista.c$(PreprocessSuffix) TDAColaLista.c


-include ../build-$(ConfigurationName)/TDAColaLista//*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(IntermediateDirectory)


