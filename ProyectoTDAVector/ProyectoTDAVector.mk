##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=ProyectoTDAVector
ConfigurationName      :=Debug
WorkspaceConfiguration := $(ConfigurationName)
WorkspacePath          :=/home/javy/Documentos/ProyectosEnC
ProjectPath            :=/home/javy/Documentos/ProyectosEnC/ProyectoTDAVector
IntermediateDirectory  :=../build-$(ConfigurationName)/ProyectoTDAVector
OutDir                 :=../build-$(ConfigurationName)/ProyectoTDAVector
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Javier Luna
Date                   :=22/05/20
CodeLitePath           :=/home/javy/.codelite
LinkerName             :=/usr/bin/g++-7
SharedObjectLinkerName :=/usr/bin/g++-7 -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../build-$(ConfigurationName)/bin/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :=$(IntermediateDirectory)/ObjectsList.txt
PCHCompileFlags        :=
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++-7
CC       := /usr/bin/gcc-7
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=../build-$(ConfigurationName)/ProyectoTDAVector/Vector.c$(ObjectSuffix) ../build-$(ConfigurationName)/ProyectoTDAVector/main.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: MakeIntermediateDirs $(OutputFile)

$(OutputFile): ../build-$(ConfigurationName)/ProyectoTDAVector/.d $(Objects) 
	@mkdir -p "../build-$(ConfigurationName)/ProyectoTDAVector"
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@mkdir -p "../build-$(ConfigurationName)/ProyectoTDAVector"
	@mkdir -p ""../build-$(ConfigurationName)/bin""

../build-$(ConfigurationName)/ProyectoTDAVector/.d:
	@mkdir -p "../build-$(ConfigurationName)/ProyectoTDAVector"

PreBuild:


##
## Objects
##
../build-$(ConfigurationName)/ProyectoTDAVector/Vector.c$(ObjectSuffix): Vector.c ../build-$(ConfigurationName)/ProyectoTDAVector/Vector.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/ProyectoTDAVector/Vector.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/Vector.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/ProyectoTDAVector/Vector.c$(DependSuffix): Vector.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/ProyectoTDAVector/Vector.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/ProyectoTDAVector/Vector.c$(DependSuffix) -MM Vector.c

../build-$(ConfigurationName)/ProyectoTDAVector/Vector.c$(PreprocessSuffix): Vector.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/ProyectoTDAVector/Vector.c$(PreprocessSuffix) Vector.c

../build-$(ConfigurationName)/ProyectoTDAVector/main.c$(ObjectSuffix): main.c ../build-$(ConfigurationName)/ProyectoTDAVector/main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/ProyectoTDAVector/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/ProyectoTDAVector/main.c$(DependSuffix): main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/ProyectoTDAVector/main.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/ProyectoTDAVector/main.c$(DependSuffix) -MM main.c

../build-$(ConfigurationName)/ProyectoTDAVector/main.c$(PreprocessSuffix): main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/ProyectoTDAVector/main.c$(PreprocessSuffix) main.c


-include ../build-$(ConfigurationName)/ProyectoTDAVector//*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(IntermediateDirectory)


