#include <stdio.h>
#include <stdlib.h>

#include "Vector.h"

void Crear(legajos* v, int tamanio)
{
    v->p = (notas*)malloc(sizeof(notas) * tamanio);

    v->tamanio = tamanio;
}

void Destruir(legajos* v)
{
    free(v->p);
}

void Cambiar(legajos* v, int posicion_legajo, int posicion_nota, float nota, float nota_promedio, int legajo)
{
    v->p[posicion_legajo].notas[posicion_nota] = nota;
    v->p[posicion_legajo].nota_promedio = nota_promedio;
    v->p[posicion_legajo].legajo = legajo;
}

float Obtener_nota(legajos v, int posicion_legajo)
{
    return v.p[posicion_legajo].nota_promedio;
}

int Obtener_legajo(legajos v, int posicion_legajo)
{
    return v.p[posicion_legajo].legajo;
}
