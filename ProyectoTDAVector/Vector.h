#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED

typedef struct VectorNotas
{
    int legajo;
    float notas[4];
    float nota_promedio; // Nota usando una funcion promedio. y evitar este dato
} notas;

typedef struct legajo
{
    int tamanio;
    notas *p;
} legajos;

void Crear(legajos*, int);

void Destruir(legajos*);

void Cambiar(legajos*, int, int, float, float, int);

float Obtener_nota(legajos, int);

int Obtener_legajo(legajos, int);

#endif
