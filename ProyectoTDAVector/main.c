#include <stdio.h>

#include "Vector.h"

int main(int argc, char** argv)
{
    int registros;
    float notas;
    int legajo;
    float nota_promedio;
    legajos v;
    printf("\n ***Programa de carga de Notas en Vector Dinamico.***\n");
    printf("\n **Se carga la cantidad de legajos**\n **y luego 4 notas por cada uno, para finalmente**\n **mostrar por pantalla los promedios mayores a un valor dado.**");
    printf("\n\n **Cuantos legajos desea cargar?: ");
    scanf("%d", &registros);
    Crear(&v, registros);
    for(int i = 0; i < registros; i++)
    {
        nota_promedio = 0;
        printf("\nCodigo del %i Legajo: ", i+1);
        scanf("%d", &legajo);

        for(int j = 0; j < 4 ; j++)
        {
            do
            {
                printf("\nCargar nota %i del legajo %i: ", j+1, legajo);
                scanf("%f", &notas);
            }
            while(notas < 0 || notas > 10);
            nota_promedio += notas;
            Cambiar(&v, i, j, notas, nota_promedio, legajo); // Cargamos cada nota con la suma de su nota promedio;
        }
        printf("\nLegajo %i: Cambiado", legajo);
    }
    printf("\nVector Cargado.");
    do
    {
        printf("\nColoque el valor del promedio minimo buscado: ");
        scanf("%f", &nota_promedio);
    }
    while(nota_promedio < 0 && nota_promedio > 10);
    for(int i = 0; i < registros; i++)
    {
        if((Obtener_nota(v, i)/4.0) >= nota_promedio)
        {
            printf("El legajo %i: tiene la nota promedio %f\n", Obtener_legajo(v, i), Obtener_nota(v, i)/4.0);
        }
    }
    Destruir(&v);
    return 0;
}
