#include <stdio.h>
#include <stdlib.h>

#include "ListaTDA.h"

void Crear(Lista_Circular *l)
{
l->Nuevo_Circular = malloc(sizeof(Nodo_Circular));
l->Nuevo_Circular = NULL;
}

void Destruir(Lista_Circular *l)
{
//free (l->Nuevo_Circular);
}

void AltaAlPrincipio(Lista_Circular *l, Nodo_Circular *n, int valor)
{
n = malloc(sizeof(Nodo_Circular));
n->valor = valor;
n->sig = l->Nuevo_Circular;
l->Nuevo_Circular = n;
}

void AltaAlFinal(Lista_Circular *l, Nodo_Circular *n, int valor)
{
Nodo_Circular *tmp = l->Nuevo_Circular;

n = malloc(sizeof(Nodo_Circular));
n->valor = valor;
n->sig = l->Nuevo_Circular;

while(l->Nuevo_Circular != tmp->sig )
{
tmp = tmp->sig;
}
tmp->sig = n;
}
/*
void BajaAlPrincipio(Lista_Circular*)


void BajaAlFinal(Lista_Circular*);

Nodo_Circular Recorrer(Lista_Circular);

void BuscarUnElemento(Lista_Circular, int);

int CantidadDeElementos(Lista_Circular);
*/
void MostrarMenu()
{
	printf("\nBienvenido al programa de listas Circulares.");
	printf("\nPor favor elija una opcion. ");
	printf("\n\n1. Crear Lista.");
	printf("\n2. Borrar Lista.");
	printf("\n3. Agregar dato al principio.");
	printf("\n4. Agregar dato al final.");
	printf("\n5. Borrar dato del principio.");
	printf("\n6. Borrar dato del final.");
	printf("\n7. Mostrar todos los elementos.");
	printf("\n8. Buscar un elemento.");
	printf("\n9. Mostrar la cantidad de elementos de la lista.");
	printf("\n\nElige una opcion: ");
}