##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=TDALista
ConfigurationName      :=Debug
WorkspaceConfiguration := $(ConfigurationName)
WorkspacePath          :=/home/javy/ProyectosC/TDALista/TDALista
ProjectPath            :=/home/javy/ProyectosC/TDALista/TDALista/TDALista
IntermediateDirectory  :=../build-$(ConfigurationName)/TDALista
OutDir                 :=../build-$(ConfigurationName)/TDALista
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Javier Luna
Date                   :=28/04/20
CodeLitePath           :=/home/javy/.codelite
LinkerName             :=/usr/bin/g++-7
SharedObjectLinkerName :=/usr/bin/g++-7 -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../build-$(ConfigurationName)/bin/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :=$(IntermediateDirectory)/ObjectsList.txt
PCHCompileFlags        :=
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++-7
CC       := /usr/bin/gcc-7
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=../build-$(ConfigurationName)/TDALista/main.c$(ObjectSuffix) ../build-$(ConfigurationName)/TDALista/up_ListaTDA.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: MakeIntermediateDirs $(OutputFile)

$(OutputFile): ../build-$(ConfigurationName)/TDALista/.d $(Objects) 
	@mkdir -p "../build-$(ConfigurationName)/TDALista"
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@mkdir -p "../build-$(ConfigurationName)/TDALista"
	@mkdir -p ""../build-$(ConfigurationName)/bin""

../build-$(ConfigurationName)/TDALista/.d:
	@mkdir -p "../build-$(ConfigurationName)/TDALista"

PreBuild:


##
## Objects
##
../build-$(ConfigurationName)/TDALista/main.c$(ObjectSuffix): main.c ../build-$(ConfigurationName)/TDALista/main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/ProyectosC/TDALista/TDALista/TDALista/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/TDALista/main.c$(DependSuffix): main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/TDALista/main.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/TDALista/main.c$(DependSuffix) -MM main.c

../build-$(ConfigurationName)/TDALista/main.c$(PreprocessSuffix): main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/TDALista/main.c$(PreprocessSuffix) main.c

../build-$(ConfigurationName)/TDALista/up_ListaTDA.c$(ObjectSuffix): ../ListaTDA.c ../build-$(ConfigurationName)/TDALista/up_ListaTDA.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/ProyectosC/TDALista/TDALista/ListaTDA.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_ListaTDA.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/TDALista/up_ListaTDA.c$(DependSuffix): ../ListaTDA.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/TDALista/up_ListaTDA.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/TDALista/up_ListaTDA.c$(DependSuffix) -MM ../ListaTDA.c

../build-$(ConfigurationName)/TDALista/up_ListaTDA.c$(PreprocessSuffix): ../ListaTDA.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/TDALista/up_ListaTDA.c$(PreprocessSuffix) ../ListaTDA.c


-include ../build-$(ConfigurationName)/TDALista//*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(IntermediateDirectory)


