#ifndef LISTATDA_H_INCLUDED
#define LISTATDA_H_INCLUDED
// Lista Circular
typedef struct Nodo_Circular {
int valor;

struct nodo *sig;
} Nodo_Circular;

typedef struct Lista_Circular
{
Nodo_Circular *Nuevo_Circular;
} Lista_Circular;

void Crear(Lista_Circular*);

void Destruir(Lista_Circular*);

void AltaAlPrincipio(Lista_Circular*, Nodo_Circular *, int);

void AltaAlFinal(Lista_Circular*, Nodo_Circular *, int);

void BajaAlPrincipio(Lista_Circular*);

void BajaAlFinal(Lista_Circular*);

Nodo_Circular Recorrer(Lista_Circular);

void BuscarUnElemento(Lista_Circular, int);

int CantidadDeElementos(Lista_Circular);

// Lista Ligada Doble

typedef struct Nodo_Doble
{
int valor;

struct Nodo_Doble *ant;
struct Nodo_Doble *sig;

} Nodo_Doble;

typedef struct Lista_Doble
{
Nodo_Doble *Nuevo_Doble;
}Lista_Doble;

#endif // LISTATDA_H_INCLUDED
