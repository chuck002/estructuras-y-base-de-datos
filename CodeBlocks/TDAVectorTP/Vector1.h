#ifndef VECTOR1_H_INCLUDED

#define VECTOR1_H_INCLUDED


typedef struct Vector

{

int *p;

int tam;

} Vector;

void Crear(Vector *,int);

void Destruir(Vector *);

void Cambiar(Vector *,int,int);

int Obtener(Vector , int);

#endif // VECTOR1_H_INCLUDED
