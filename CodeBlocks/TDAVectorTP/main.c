#include <stdio.h>
#include <stdlib.h>
#include "Vector1.h"

int main()
{
    Vector v;
    int i,dato,tam;
    printf("Ingrese el tamaño del vector: ");
    scanf("%d",&tam);
    Crear(&v,tam);
    printf("\nCarga del vector");
    for(i=0;i<tam;i++){
        printf("\nIngrese el valor para V[%d]",i);
        scanf("%d",&dato);
        Cambiar(&v,i,dato);
    }
    printf("\nEmision del vector");
    for(i=0;i<tam;i++){
        printf("v[%d]=%d",i,Obtener(v,i));
    }
    printf("\nIndique posicion y valor a cambiar, -1 para fin");
    printf("\nPosicion: ");
    scanf("%d",&i);
    while(i!=-1){
        printf("\nIndique nuevo valor");
        scanf("%d",&dato);
        Cambiar(&v,i,dato);
        printf("\n Posicion");
        scanf("%d",&i);
    }
    printf("\nEmision del vector");
    for(i=0;i<tam;i++){
        printf("\nV[%d]=%d",i,Obtener(v,i));
    }
    printf("\nADIOS");
    Destruir(&v);
    return 0;
}
