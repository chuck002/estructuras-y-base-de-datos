#ifndef LISTATDADOBLE_H_INCLUDED
#define LISTATDADOBLE_H_INCLUDED

typedef struct Nodo{
	struct Nodo *sig, *ant;
	int valor;
}nodo;

typedef struct Lista{
	nodo *lis; 
}lista;

void Crear(lista *);

void Destruir(lista *);

void AltaFrente(lista *, int);

void AltaFinal(lista *, int);

int Buscar(lista, int);

void Recorre(lista);

void Borrar(lista *, int);

#endif // LISTATDA_H_INCLUDED
