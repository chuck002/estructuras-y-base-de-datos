#ifndef HASHING_H_INCLUDED
#define HASHING_H_INCLUDED


typedef struct hashing{

    int legajo;
    char nombre[15];
    char apellido[15];
    char categoria;
    int activo;

} myhash;


void menu();

void cargarHashACero(myhash[]);

int ColisionCuadratica(int ,int ,int);

int CreaHash(int, int);

void Almacenar(myhash[], int);

void Buscar(myhash[], int);

void Borrar(myhash[], int);

void Listar(myhash[]);

double cuadrado(double);

int convertirCadena(char[]);

int tamanioCadena(char[]);

void sacarSaltoDeLinea(char[]);

#endif // HASHING_H_INCLUDED
