#include "hashing.h"
#include <math.h>
#include <stdio.h>
#include <stdio_ext.h>
#include <string.h>
#include <stdlib.h>

#define TAM 100

void menu()
{
    printf("\n<<<--- HASHING --->>>\n\n");
    printf("1. Agregar registro.\n");
    printf("2. Buscar registro.\n");
    printf("3. Eliminar registro.\n");
    printf("4. Listar registros.\n");
    printf("5. Salir.\n");
}

// FUNCION HASH devuelve un HASH creado por la formula
// de plegado de un numero entero de 6 digitos (el legajo).
// En 3 partes de dos digitos y se las multiplica entre si,
// para lograr una constante para la funcion.

int CreaHash(int tam, int legajo)
{
    int a, b, c;
    int k;
    a = legajo % 100;
    b = ((legajo - a) % 10000);
    c = (((legajo - a) )-b)/10000;
    b = b/100;
    k = a * b * c;
    return k % tam;
}
int ColisionCuadratica(int pos, int k, int tam)
{
    return (int) (pos + ((int)cuadrado((double)k)));
}

void cargarHashACero(myhash v[TAM])
{
    for(int i = 0; i < TAM; i++)
    {
        v[i].legajo = 0;
        v[i].activo = 0;
    }
}

// Se carga el hash correspondiente verificando si se produce o no una colision.
// Si es asi se activa el algoritmo para saltar esta colision, sino se carga el dato.

void Almacenar(myhash v[TAM], int legajo)
{
    myhash tmp;


    int pos = CreaHash(TAM, legajo);

    if(tmp.legajo == v[pos].legajo)
    {
        printf("\nEl legajo ya se encuentra en la base de datos.");
    }
    else
    {
        tmp.legajo = legajo;
        tmp.activo = 1;
        printf("\n--- Registro %d ---", legajo);
        printf("\nNombre: ");

        fgets(tmp.nombre, sizeof(tmp.nombre), stdin);
        sacarSaltoDeLinea(tmp.nombre);

        printf("\nApellido: ");
        fgets(tmp.apellido, sizeof(tmp.apellido), stdin);
        sacarSaltoDeLinea(tmp.apellido);

        do
        {
            __fpurge(stdin);
            printf("\nCategoria (a,b,c): ");
            scanf("%c", &tmp.categoria);
        }
        while(tmp.categoria != 'a' && tmp.categoria != 'b' && tmp.categoria != 'c');

        printf("%s, %s, %d", v[pos].nombre, v[pos].apellido, v[pos].legajo );


        if(v[pos].legajo == 0)
        {
            v[pos] = tmp;
        }
        else
        {

            int i = pos;
            int k = 1;
            do
            {
                pos = ColisionCuadratica(i, k, TAM) % TAM ;

                if(v[pos].legajo == 0)
                {
                    v[pos] = tmp;
                    break;

                }
                k++;

            }
            while(v[pos].legajo != 0 && k < TAM );
            if(i == TAM)
            {
                printf("No hay lugar para el nuevo registro.");

            }
        }
    }
}

void Buscar(myhash v[TAM], int legajo)
{
    int pos = CreaHash(TAM, legajo);
    int k = 0;
    int i = 0;

    if(v[pos].legajo == 0 && v[pos].activo == 0)
    {
        printf("El registro no esta en la tabla.");

    }
    else if(v[pos].legajo == legajo && v[pos].activo == 1)
    {
        printf("El registro esta en la posicion: %d\n", pos);
        printf("Legajo: %d\n", v[pos].legajo);
        printf("Nombre Completo: %s %s\n", v[pos].nombre, v[pos].apellido);
        printf("Categoria: %c\n", v[pos].categoria);
        printf("Activo: SI\n");
    }
    else
    {

        do
        {
            k = ColisionCuadratica(pos, i, TAM) % TAM ;
            if(v[k].legajo == legajo)
            {
                printf("El registro esta en la posicion: %d\n", k);
                printf("Legajo: %d\n", v[k].legajo);
                printf("Nombre Completo: %s %s\n", v[k].nombre, v[k].apellido);
                printf("Categoria: %c\n", v[k].categoria);
                printf("Activo: SI\n");
                break;
            }
            printf("\n%valor: %d\n", k);
            k = 0;
            i++;
        }
        while(v[k].legajo != 0 || i < TAM);

    }
}

void Borrar(myhash v[TAM], int legajo)
{
    int pos = CreaHash(TAM, legajo);
    int i = 0;
    int k = 0;
    if(v[pos].legajo == 0 && v[pos].activo == 0)
    {
        printf("El registro no esta en la tabla.");

    }
    else if(v[pos].legajo == legajo && v[pos].activo == 1)
    {
        v[pos].activo = 0;
    }
    else
    {

        do
        {
            k = ColisionCuadratica(pos, i, TAM) % TAM ;
            if(v[k].legajo == legajo && v[k].activo == 1)
            {
                v[k].activo = 0;
                break;
            }
            i++;
        }
        while(v[k].legajo != 0 || i < TAM);

    }
}

void Listar(myhash v[TAM])
{
    int i = 0;

    do
    {
        if(v[i].legajo != 0 && v[i].activo == 1)
        {
            char cadena[5];
            if(v[i].activo == 1)
            {
                strcpy(cadena, "SI");
            }
            else
            {
                strcpy(cadena, "NO");
            }
            printf("[%d]Legajo: %d, Nombre Completo: %s %s, Categoria: %c, Activo: %s\n", i, v[i].legajo, v[i].nombre, v[i].apellido, v[i].categoria, cadena);
        }
        i++;
    }
    while(i < TAM);
}

double cuadrado(double numero)
{
    return numero*numero;
}

int convertirCadena(char cadena[])
{
    return atoi(cadena);
}

int tamanioCadena(char cadena[])
{
    return sizeof(cadena)-2;
}

void sacarSaltoDeLinea(char cadena[])
{
    int tam;
    tam = strlen(cadena);
    if(cadena[tam-1] == '\n')
    {
        cadena[tam-1] = '\0';
    }
}

void cargarStructEnArchivo(myhash v[TAM], int pos)
{
    FILE *fichero;
    fichero = fopen("TablaEmpleados.txt", "wt");
    fprintf(fichero, "%d, %d, %s, %s, %c, %d", pos, v[pos].legajo, v[pos].nombre, v[pos].apellido, v[pos].categoria, v[pos].activo);
    fichero = fclose(fichero);
}

myhash recibirStructEnArchivo(FILE *fichero)
{
    myhash hash;
    fichero = fopen("TablaEmpleados.txt", "rd");


    fprintf(fichero, "%d,%d,%s,%s,%c,%d", pos, v[pos].legajo, v[pos].nombre, v[pos].apellido, v[pos].categoria, v[pos].activo);
    fichero = fclose(fichero);
}

