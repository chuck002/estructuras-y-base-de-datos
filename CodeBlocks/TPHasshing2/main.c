#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
#include "hashing.h"

#define TAM 100

int main()
{
    myhash hash[TAM];
    cargarHashACero(hash);
    menu();
    int op;
    int tamCadena;
    char legajo[30];

    do
    {
        printf("Opcion: ");
        scanf("%d", &op);
        switch(op)
        {
        case 1:
            do
            {
                __fpurge(stdin);

                printf("Legajo (Min 111111, Max 999999): ");
                fgets(legajo, sizeof legajo, stdin);
                sacarSaltoDeLinea(legajo);
            }
            while(convertirCadena(legajo) < 111111 || convertirCadena(legajo) > 999999);

            Almacenar(hash, convertirCadena(&legajo));
            break;
        case 2:
            do
            {
                __fpurge(stdin);
                printf("Colocar el legajo a buscar (Min 111111, Max 999999): ");
                fgets(legajo, sizeof(legajo), stdin);
                sacarSaltoDeLinea(legajo);
                Buscar(hash, convertirCadena(&legajo));
                int pos1 = CreaHash(TAM, convertirCadena(&legajo));
            }
            while(convertirCadena(legajo) < 111111 || convertirCadena(legajo) > 999999);

            break;
        case 3:
            do
            {
                __fpurge(stdin);

                printf("Legajo a Borrar (Min 111111, Max 999999): ");
                fgets(legajo, sizeof legajo, stdin);
                sacarSaltoDeLinea(legajo);
            }
            while(convertirCadena(legajo) < 111111 || convertirCadena(legajo) > 999999);
            Borrar(hash, convertirCadena(&legajo));
            break;
        case 4:
            Listar(hash);
            break;
        case 5:
            return 0;
            break;
        }
        menu();
    }
    while(op != 5);
    return 0;
}
