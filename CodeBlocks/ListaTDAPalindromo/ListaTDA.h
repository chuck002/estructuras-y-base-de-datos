#ifndef LISTATDA_H_INCLUDED
#define LISTATDA_H_INCLUDED

typedef struct Nodo
{
    char letra;
    struct Nodo *sig;
} nodo;

typedef struct Pila
{
    nodo *p;
} pila;

void Crear(pila*);

void Destruir(pila*);

void Apilar(pila*, char);

void Desapilar(pila*);

char Tope(pila);

int Vacia(pila);

#endif // LISTATDA_H_INCLUDED
