#include "hashing.h"
#include <math.h>
#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>

void menu()
{
    printf("<<<--- HASHING --->>>\n\n");
    printf("1. Agregar registro.\n");
    printf("2. Buscar registro.\n");
    printf("3. Eliminar registro.\n");
    printf("4. Listar registros.\n");
    printf("5. Salir.\n");
}

// FUNCION HASH devuelve un HASH creado por la formula
// de plegado de un numero entero de 6 digitos (el legajo).
// En 3 partes de dos digitos y se las multiplica entre si,
// para lograr una constante para la funcion.

int CreaHash(int tam, int legajo)
{
    int a, b, c;
    int k;
    a = legajo % 100;
    b = ((legajo - a) % 10000);
    c = (((legajo - a) )-b)/10000;
    b = b/100;
    k = a * b * c;
    return k % tam;
}
int ColisionCuadratica(int pos, int k, int tam)
{
    return (int) (pos+ ((int)cuadrado((double)k) % tam));
}

// Se carga el hash correspondiente verificando si se produce o no una colision.
// Si es asi se activa el algoritmo para saltar esta colision, sino se carga el dato.

void Almacenar(vectorhash *v, int legajo)
{
    myhash *tmp = malloc(sizeof(myhash));
    tmp->legajo = legajo;
    tmp->activo = 1;

    printf("\n--- Registro %d ---", legajo);
    printf("\nNombre: ");

    fgets(tmp->nombre, sizeof(tmp->nombre), stdin);
    sacarSaltoDeLinea(tmp->nombre);

    printf("\nApellido: ");
    fgets(tmp->apellido, sizeof(tmp->apellido), stdin);
    sacarSaltoDeLinea(tmp->apellido);

    printf("\nCategoria (a,b,c): ");

    scanf("%c", &tmp->categoria);

    int pos = CreaHash(100, legajo);

    //v->legajos[pos] = tmp;

    printf("%s, %s, %d", v->legajos[pos]->nombre, v->legajos[pos]->apellido, v->legajos[pos]->legajo );


    if(v->legajos[pos]->nombre[0] == '\0' && v->legajos[pos]->apellido[0] == '\0')
    {
        v->legajos[pos] = tmp;
    }
    else
    {

        int i = pos;
        printf("\nEntro al primer if\n");
        do
        {
            pos = ColisionCuadratica(pos, i, 100);

            printf("\nEntro al do-while %d position: %d\n", i, pos);
            i++;
        }
        while(v->legajos[pos] != NULL && i < 100);
        if(i<100)
        {
            v->legajos[pos] = tmp;
        }
        else
        {
            printf("No hay lugar para el nuevo registro.");

        }
    }
}

void CargarVector(vectorhash *v, int tam)
{
    v->legajos[tam] = malloc(sizeof (myhash) * tam);

}

int Buscar(vectorhash *v, int legajo)
{
    int pos = CreaHash(100, legajo);
    if(v->legajos[pos] == NULL)
    {
        printf("El registro no esta en la tabla.");
        return 0;
    }
    else
    {

        int k = pos;
        while((v->legajos[pos]->legajo != legajo || v->legajos[pos] != NULL) && v->legajos[pos]->activo == 1)
        {
            pos = ColisionCuadratica(pos, k, 100);
            k++;
        }
        if(v->legajos[pos] == NULL)
        {
            printf("El registro no esta.");
            return 0;
        }
        else
        {
            printf("El registro esta.");
            return 1;
        }
    }
}

void Borrar(vectorhash *, int legajo);

void Listar(vectorhash *v)
{
    int i = 0;
    char activo[5];
    do
    {
        if(v->legajos[i]->legajo != NULL)
        {
            if(v->legajos[i]->activo == 1)
            {
                activo[5] = "Si";
            }
            else
            {
                activo[5] = "No";
            }
            printf("Legajo: %d, Nombre Completo: %s %s, Categoria: %c, Activo: %s", v->legajos[i]->legajo, v->legajos[i]->nombre, v->legajos[i]->apellido, v->legajos[i]->categoria, activo);
        }
        i++;
    }
    while(i < 100);
}

double cuadrado(double numero)
{
    return numero*numero;
}

int convertirCadena(char cadena[])
{
    return atoi(cadena);
}

int tamanioCadena(char cadena[])
{
    return sizeof(cadena)-2;
}

void sacarSaltoDeLinea(char cadena[])
{
    int tam;
    tam = strlen(cadena);
    if(cadena[tam-1] == '\n')
    {
        cadena[tam-1] = '\0';
    }
}

