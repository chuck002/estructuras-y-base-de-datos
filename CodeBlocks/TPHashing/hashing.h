#ifndef HASHING_H_INCLUDED
#define HASHING_H_INCLUDED


typedef struct hashing{

    int legajo;
    char nombre[15];
    char apellido[15];
    char categoria;
    int activo;

} myhash;

typedef struct Vector{
  myhash *legajos[100];
}vectorhash;

void menu();

void CargarVector(vectorhash *, int);

int ColisionCuadratica(int ,int ,int);

int CreaHash(int, int);

void Almacenar(vectorhash *, int);

int Buscar(vectorhash *, int);

void Borrar(vectorhash *, int);

void Listar(vectorhash *);

double cuadrado(double);

int convertirCadena(char[]);

int tamanioCadena(char[]);

void sacarSaltoDeLinea(char[]);

#endif // HASHING_H_INCLUDED
