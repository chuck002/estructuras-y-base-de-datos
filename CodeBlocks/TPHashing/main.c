#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
#include "hashing.h"

int main()
{
    vectorhash vector;
    CargarVector(&vector, 100);
    menu();
    int op;
    int tamCadena;
    char legajo[30];

    do
    {
        scanf("%d", &op);
        switch(op)
        {
        case 1:
            __fpurge(stdin);
            printf("Legajo (Min 111111, Max 999999): ");
            fgets(legajo, sizeof legajo, stdin);
            sacarSaltoDeLinea(legajo);
            Almacenar(&vector, convertirCadena(&legajo));
            break;
        case 2:
            printf("Colocar el legajo a buscar: ");
            fflush(stdin);
            fgets(legajo, sizeof(legajo), stdin);
            if(tamanioCadena(legajo) == 6)
            {
                Buscar(&vector, convertirCadena(&legajo));
            }
            else
            {
                printf("El legajo debe contener 6 digitos.");
            }

            break;
        case 3:
            //Borrar(registros, legajo);
            break;
        case 4:
            Listar(&vector);
            break;
        case 5:
            return 0;
            break;
        }
        menu();
    }
    while(op != 5);
    return 0;
}
