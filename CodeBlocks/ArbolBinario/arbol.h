#ifndef ARBOL_H_INCLUDED
#define ARBOL_H_INCLUDED

typedef struct Nodo
{
    nodo *izq;
    nodo *der;
    int valor;
} nodo;

typedef struct ARBOL_H_INCLUDED{
nodo * raiz;

} arbol;

void CrearArbol(arbol *);

void AltaNodo(arbol*, int);

int ContarNodos(arbol);

void RecorrerArbol(arbol);

void BuscarPreOrden(arbol);

void BuscarPostOrden(arbol);

#endif // ARBOL_H_INCLUDED
