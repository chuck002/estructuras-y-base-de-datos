#ifndef PALABRA_H_INCLUDED
#define PALABRA_H_INCLUDED

typedef struct Vector
{
    int pos_primero;
    int pos_ultimo;
    int *pos;
    int tamanio;
} cola;

void Crear(cola*, int);

void Destruir(cola*);

void Encolar(cola*, char);

void Desencolar(cola*);

char Tope(cola);

char Fondo(cola);

int Vacia(cola);


#endif
