#include <stdio.h>
#include <stdlib.h>

#include "Palabra.h"

#define TAMANIO 6

void Crear(cola *c, int tam)
{
    c->pos = (int*)malloc(sizeof(int)* tam);
    c->tamanio = tam;
    c->pos_primero = -1;
    c->pos_ultimo = -1;

}

void Destruir(cola* c)
{
    free(c->pos);
}

void Encolar(cola *c, char valor)
{
    if(c->pos_primero == -1)
    {
        c->pos_primero = 0;
        c->pos_ultimo = 0;
        c->pos[c->pos_primero] = valor;
        c->pos[c->pos_ultimo] = valor;
        //printf("\nEncolo primer elemento.");
    }
    else
    {
        if(c->pos_ultimo < c->tamanio-1)
        {
            c->pos_ultimo++;
            c->pos[c->pos_ultimo] = valor;
        }
        else
        {
            if((c->pos_ultimo - c->pos_primero)+1 == c->tamanio)
            {
                printf("\nEl Vector se encuentra completo. Debe esperar a que se vacie.");
            }
            else
            {
                c->pos_ultimo = 0;
                c->pos[c->pos_ultimo] = valor;
            }
        }
    }
}

void Desencolar(cola* c)
{
    if(c->pos_primero == -1)
    {
        printf("\nNo hay datos para retirar de la cola.");
    }
    else
    {
        if(c->pos_primero != c->pos_ultimo && c->pos_primero != c->tamanio-1)
        {
            c->pos_primero++;
        }
        else if(c->pos_primero == c->pos_ultimo)
        {
            c->pos_primero = -1;
        }
        else
        {
            c->pos_primero = -1;
            c->pos_ultimo = -1;
        }
    }
}

char Tope(cola c)
{
    if(c.pos_primero == -1)
    {
        printf("\nNo hay datos cargados en la cola.");
        return 0;
    }
    else
    {
        return c.pos[c.pos_primero];
    }
}

char Fondo(cola c)
{
    if(c.pos_primero == -1)
    {
        printf("\nNo hay datos cargados en la cola.");
        return 0;
    }
    else
    {
        return c.pos[c.pos_ultimo];
    }
}

int Vacia(cola c)
{
    if(c.pos_primero == -1)
    {
        return 1; // Retorna 1 si esta vacia.
    }
    else
    {
        return 0; // Retorna 0 si esta llena.
    }
}
