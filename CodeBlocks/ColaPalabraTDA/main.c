#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Palabra.h"

int main(int argc, char **argv)
{
    int tam;
    int i = 0;
    int esPal = 1; // 0 si tiene espacios en medio.
    int esVocal = 0;
    int esPalabraEquivocada = 0; // 0 si no posee ni vocales juntas, ni consonantes juntas y es palabra correcta.
    int orden = 0; // 0 para Ordenada, 1 para Desordenada
    cola c;
    char *salto;

    printf("Programa de reconocimientos de palabras.");
    printf("\nColoca el tamanio del array que deseas usar: ");
    scanf("%d", &tam);
    char palabra[tam];
    char palabra_en_cola[tam];
    while((tam = getchar()) != '\n' && tam != EOF);
    Crear(&c, tam);
    printf("\nColoca el valor de la cadena: ");
    fgets(palabra, sizeof(palabra), stdin);
    salto = strchr(palabra, 10);
    if(salto != NULL)
    {
        *salto = '\0';
    }
    do
    {
        Encolar(&c, palabra[i]); //Encolamos cada char del vector.
        i++;
    }
    while(palabra[i] != '\0');

    int j = 0;
    do
    {
        palabra_en_cola[j] = Tope(c);
        Desencolar(&c);     //Sacamos cada caracter de la cola y desencolamos para ver el siguiente tope.
        j++;
    }
    while(Vacia(c) != 1);
    i = 0;
    do
    {
        if(strcmp(&palabra[0], &palabra_en_cola[i]) > 0
                && palabra_en_cola[i] != ' '
                && orden == 0
                && palabra[0] != palabra_en_cola[i])// Comprobamos si las letras estan ordenadas alfabeticamente.
        {
            orden = 1;
        }
        i++;
    }
    while(palabra_en_cola[i] != '\0');

    j = 0;
    do
    {
        if(palabra_en_cola[j] == ' ') //Comprobamos si es una palabra unica, recorriendo el array y buscando un caracter espacio.
        {
            esPal = 0;
        }
        j++;

    }
    while(palabra_en_cola[j] != '\0');

    i = 0;
    do
    {

            if(((palabra_en_cola[i] == 'a')||
               (palabra_en_cola[i] == 'e')||
               (palabra_en_cola[i] == 'i')||    //Comprobamos si es vocal y luego comprobamos si el siguiente caracter tambien es una vocal.
               (palabra_en_cola[i] == 'o')||
               (palabra_en_cola[i] == 'u')) && palabra_en_cola[i] != '\n')
            {
            if((palabra_en_cola[i+1] == 'a')||
               (palabra_en_cola[i+1] == 'e')||
               (palabra_en_cola[i+1] == 'i')||
               (palabra_en_cola[i+1] == 'o')||
               (palabra_en_cola[i+1] == 'u'))
               {
                   esPalabraEquivocada = 1;
               }

            }else
            {
            if((palabra_en_cola[i] != 'a')&&
               (palabra_en_cola[i] != 'e')&&
               (palabra_en_cola[i] != 'i')&&    //Comprobamos si es consonante y luego comprobamos si el siguiente caracter tambien es una consonante.
               (palabra_en_cola[i] != 'o')&&
               (palabra_en_cola[i] != 'u'))
               {
            if((palabra_en_cola[i+1] != 'a')&&
               (palabra_en_cola[i+1] != 'e')&&
               (palabra_en_cola[i+1] != 'i')&&
               (palabra_en_cola[i+1] != 'o')&&
               (palabra_en_cola[i+1] != 'u'))
               {
                   esPalabraEquivocada = 1;
               }
               }
            }
    i++;
    }
    while(palabra_en_cola[i] != '\0');

    if(orden != 0)
    {
        printf("\nLa cadena esta DESORDENADA alfabeticamente.");
    }
    else
    {
        printf("\nLa cadena esta ORDENADA alfabeticamente.");
    }

    if(esPal != 1)
    {
        printf("\nNO es una palabra UNICA");
    }
    else if( esPal == 1 && esPalabraEquivocada == 1)
    {
        printf("\nEs palabra UNICA, pero NO tiene FORMA CORRECTA.\n(Tiene 2 vocales o 2 consonantes juntas).");

    }
    else
    {
        printf("\nEs Palabra UNICA y tiene FORMA CORRECTA.");
    }

    Destruir(&c);
    return 0;
}
