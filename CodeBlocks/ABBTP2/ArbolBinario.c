#include <stdlib.h>
#include <stdio.h>

#include "ArbolBinario.h"



/*  Realizar en C un menú que tenga las siguientes operaciones fundamentales:
    1) Crear un árbol.
    2) Dar el alta en un árbol.
    3) Recorrer un árbol: preorden -inorden – postorden.
    4) Esta, un dato en un árbol.
    5) Cantidad de nodos en árbol.
    6) Altura de un árbol.*/

void MenuPrincipal()
{
    printf("\n<<<--- ARBOL BINARIO DE BUSQUEDA --->>>");
    printf("\n\n1) Crear Arbol Binario.");
    printf("\n2) Dar el Alta en el Arbol.");
    printf("\n3) Recorrer el Arbol: \n      (A) - PreOrden.\n      (B) - InOrden.\n      (C) - PostOrden.");
    printf("\n4) Buscar si se encuentra un Dato.");
    printf("\n5) Cantidad de Nodos del Arbol.");
    printf("\n6) Altura total del Arbol.");
    printf("\n7) Borrar Arbol.");
    printf("\n8) Busca un dato y suma los datos derechos.");
    printf("\n9) Crear dos Arboles y buscar los del primero que no estan en el segundo.");
    printf("\n10) Crea dos Arboles uno de vendedor y otro de ventas y devuelve una lista enlazada con el nombre y le vendedor.");
    printf("\n11) Salir.");
}
int recorre = 0;

// 1) Crear un árbol.
void CrearArbol(Arbol *arb)
{
    arb->raiz = NULL;
}

// 2) Dar el alta en un árbol.

void AltaDato(Arbol *arb, int dato)
{
    abb *nuevo;
    nuevo = malloc(sizeof(abb));

    nuevo->dato = dato;
    nuevo->izq = NULL;
    nuevo->der = NULL;

    if(arb->raiz == NULL)
    {
        arb->raiz = nuevo;
    }
    else
    {
        abb *anterior, *recorre;
        anterior = NULL;
        recorre = arb->raiz;
        while(recorre != NULL)
        {
            anterior = recorre;
            if(dato < recorre->dato)
            {
                recorre = recorre->izq;
            }
            else
            {
                recorre = recorre->der;
            }
        }
        if(dato < anterior->dato)
        {
            anterior->izq = nuevo;
        }
        else
        {
            anterior->der = nuevo;
        }
    }

}

// 3) Recorrer un árbol:
// PREORDEN.
void RecorrerPreorden(Arbol *arb)
{
    if(arb->raiz == NULL)
    {
        //printf("\nNo hay nada que mostrar en el arbol. Esta VACIO.");
    }
    else
    {
        printf("\nA) PreOrden - Dato: %i", (arb->raiz->dato));
        RecorrerPreorden(&arb->raiz->izq);
        RecorrerPreorden(&arb->raiz->der);
    }
}

// INORDEN
void RecorrerInOrden(Arbol *arb)
{
    if(arb->raiz == NULL)
    {
        //printf("\nNo hay nada que mostrar en el arbol. Esta VACIO.");
    }
    else
    {
        RecorrerInOrden(&arb->raiz->izq);
        printf("\nB) InOrden - Dato: %i", arb->raiz->dato);
        RecorrerInOrden(&arb->raiz->der);
    }

}

// POSTORDEN
void RecorrerPostOrden(Arbol *arb)
{
    if(arb->raiz == NULL)
    {
        //printf("\nNo hay nada que mostrar en el arbol. Esta VACIO.");
    }
    else
    {
        RecorrerPostOrden(&arb->raiz->izq);
        RecorrerPostOrden(&arb->raiz->der);
        printf("\nC) PostOrden - Dato: %i", arb->raiz->dato);
    }
}

// 4) Esta, un dato en un árbol.
void EstaDato(Arbol *arb, int dato)
{
    if(arb->raiz == NULL)
    {
        printf("\nEl dato %i, no esta en el arbol.", dato);
    }
    else
    {
        if(arb->raiz->dato == dato)
        {
            printf("\nEl dato %i, esta en el arbol.", dato);
        }
        else if(dato > arb->raiz->dato)
        {
            EstaDato(&arb->raiz->der, dato);
        }
        else if(dato < arb->raiz->dato)
        {
            EstaDato(&arb->raiz->izq, dato);
        }
    }
}

// 5) Cantidad de nodos en árbol.
int CantidadNodos(Arbol *arb)
{
    if(arb->raiz == NULL)
    {
        return 0;
    }
    else
    {
        return 1+CantidadNodos(&arb->raiz->izq)+CantidadNodos(&arb->raiz->der);
    }

}

// 6) Altura del Arbol
int CantidadNiveles(Arbol *arb)
{
    int izq = 0;
    int der = 0;
    if(arb->raiz == NULL)
    {
        return 0;
    }
    else
    {
        izq = CantidadNiveles(&arb->raiz->izq);
        der = CantidadNiveles(&arb->raiz->der);

        if(izq == der)
        {
            return 1 + der;
        }
        else if (izq > der)
        {
            return izq +1;
        }
        else
        {
            return der +1;
        }
    }
}

// Borrar Arbol
void BorrarArbol (Arbol *arb)
{
    if (arb->raiz != NULL)
    {
        BorrarArbol(&arb->raiz->izq);
        BorrarArbol(&arb->raiz->der);
        free (arb->raiz);
        arb->raiz = NULL;

    }
}

/*
g. Escribir una función que muestre la suma de valores  mayores a partir de un valor x.
*/
int SumaDer(Arbol *arb, int dato)
{
    abb *recorre = arb->raiz;
    int total = 0;
    if(recorre == NULL)
    {
        //printf("\nEl arbol esta vacio.");
        return total;
    }
    else
    {
        if(recorre->dato == dato)
        {
            while(recorre->der != NULL)
            {
                total += recorre->der->dato;
                recorre = recorre->der;
            };
            return total;
        }
        else
        {
            if(dato > recorre->dato)
            {
                return total+SumaDer(&recorre->der, dato);
            }
            else if(dato < recorre->dato)
            {
                return total+SumaDer(&recorre->izq, dato);
            }
        }
    }
}
/*
h. Escribir una función que reciba dos punteros de entrada a 2 árboles distintos y retorne un árbol con
los elementos que están en un árbol 1 y no en el árbol 2.
*/

int EstaDatoArbol(Arbol *arb, int dato)
{
    if(arb->raiz == NULL)
    {
        //printf("\nEl dato %i, no esta en el arbol.", dato);
        return 0;
    }
    else
    {
        if(arb->raiz->dato == dato)
        {
            return 1;
        }
        else if(dato > arb->raiz->dato)
        {
            EstaDatoArbol(&arb->raiz->der, dato);
        }
        else if(dato < arb->raiz->dato)
        {
            EstaDatoArbol(&arb->raiz->izq, dato);
        }
    }
}
void RecorreryBuscar(Arbol *arbDato, Arbol *arbBusqueda, Arbol *arbReturn)
{
    if(arbDato->raiz == NULL)
    {
        //printf("\nNo hay nada que mostrar en el arbol. Esta VACIO.");
    }
    else
    {
         printf("\n Entro al else dato");
        if(EstaDatoArbol(arbBusqueda, arbDato->raiz->dato) == 0)
        {
            printf("\n Entro a cargar dato");
            AltaDato(arbReturn, arbDato->raiz->dato);
        }
        RecorreryBuscar(&arbDato->raiz->der, arbBusqueda, arbReturn);
        RecorreryBuscar(&arbDato->raiz->izq, arbBusqueda, arbReturn);
    }
}


/*
i. Escribir una función que reciba dos punteros de entrada a 2 árboles distintos.
Escribir una función que recibe
árbol 1
struct
{
    cod_vendedor 	int
     nombre    char
     nodo *izq, *der
}
	 árbol 2
	 struct
{
    cod_vendedor int
    ventas 	 float
    nodo *izq, der
}
Y debe devolver una lista ligada simple con el nombre_vendedor, y la venta Max de cada vendedor.
*/
