#include <stdlib.h>
#include <stdio.h>

#include "ArbolEmpleados.h"
#include "ListaVendedores.h"

void CrearArbolVendedor(nom *arb)
{
    arb->raiz = NULL;
}
void CrearArbolVenta(vent *arb)
{
    arb->raiz = NULL;
}

void AltaDatoVendedor(nom *arb, int dato, char const *nombre)
{
    nombre_ven *nuevo;
    nuevo = malloc(sizeof(nombre_ven));

    nuevo->nombre = malloc(strlen(nombre)+1);
    strcpy(nuevo->nombre, nombre);

    nuevo->cod_vendedor = dato;
    nuevo->izq = NULL;
    nuevo->der = NULL;

    if(arb->raiz == NULL)
    {
        arb->raiz = nuevo;
    }
    else
    {
        nombre_ven *anterior, *recorre;
        anterior = NULL;
        recorre = arb->raiz;
        while(recorre != NULL)
        {
            anterior = recorre;
            if(dato < recorre->cod_vendedor)
            {
                recorre = recorre->izq;
            }
            else
            {
                recorre = recorre->der;
            }
        }
        if(dato < anterior->cod_vendedor)
        {
            anterior->izq = nuevo;
        }
        else
        {
            anterior->der = nuevo;
        }
    }

}
void AltaDatoVentas(vent *arb, int dato, float venta)
{
    venta_ven *nuevo;
    nuevo = malloc(sizeof(venta_ven));

    nuevo->cod_vendedor = dato;
    nuevo->ventas = venta;
    nuevo->izq = NULL;
    nuevo->der = NULL;

    if(arb->raiz == NULL)
    {
        arb->raiz = nuevo;
    }
    else
    {
        venta_ven *anterior, *recorre;
        anterior = NULL;
        recorre = arb->raiz;
        while(recorre != NULL)
        {
            anterior = recorre;
            if(dato < recorre->cod_vendedor)
            {
                recorre = recorre->izq;
            }
            else
            {
                recorre = recorre->der;
            }
        }
        if(dato < anterior->cod_vendedor)
        {
            anterior->izq = nuevo;
        }
        else
        {
            anterior->der = nuevo;
        }
    }
}

int EstaDatoVendedor(nom *arb, int dato)
{
    if(arb->raiz == NULL)
    {
        //printf("\nEl dato %i, no esta en el arbol.", dato);
        return 0;
    }
    else
    {
        if(arb->raiz->cod_vendedor == dato)
        {
            return 1;
        }
        else if(dato > arb->raiz->cod_vendedor)
        {
            EstaDatoVendedor(&arb->raiz->der, dato);
        }
        else if(dato < arb->raiz->cod_vendedor)
        {
            EstaDatoVendedor(&arb->raiz->izq, dato);
        }
    }
}
int EstaDatoVentas(vent *arb, int dato)
{
    if(arb->raiz == NULL)
    {
        //printf("\nEl dato %i, no esta en el arbol.", dato);
        return 0;
    }
    else
    {
        if(arb->raiz->cod_vendedor == dato)
        {
            return 1;
        }
        else if(dato > arb->raiz->cod_vendedor)
        {
            EstaDatoVentas(&arb->raiz->der, dato);
        }
        else if(dato < arb->raiz->cod_vendedor)
        {
            EstaDatoVentas(&arb->raiz->izq, dato);
        }
    }
}

float RecibeDatoVenta(vent *arbVent, int dato)
{

    if(arbVent->raiz == NULL)
    {
        //printf("\nEl dato %i, no esta en el arbol.", dato);
        return 0.0;
    }
    else
    {
        if(arbVent->raiz->cod_vendedor == dato)
        {
            return arbVent->raiz->ventas;
        }
        else if(dato > arbVent->raiz->cod_vendedor)
        {
            RecibeDatoVenta(&arbVent->raiz->der, dato);
        }
        else if(dato < arbVent->raiz->cod_vendedor)
        {
            RecibeDatoVenta(&arbVent->raiz->izq, dato);
        }
    }
}
void CrearListaVendedores(nom *ArbVendedor, vent *ArbVentas, Entrada *ListVentas)
{
    float ventaTmp = 0;
    vent *recorre;
    recorre = ArbVentas->raiz;
    if(ArbVendedor->raiz == NULL)
    {
        //printf("\nNo hay datos de los VENDEDORES para mostrar.");
    }
    else
    {
        if(EstaDatoVendedor(ArbVendedor, ArbVendedor->raiz->cod_vendedor) == 1 && EstaDatoVentas(ArbVentas, ArbVendedor->raiz->cod_vendedor) == 1 )
        {

            ventaTmp = RecibeDatoVenta(ArbVentas, ArbVendedor->raiz->cod_vendedor);
            printf("\nVenta: %f, %i", ventaTmp, ArbVendedor->raiz->cod_vendedor);
            AltaNodoLista(ListVentas, ArbVendedor->raiz->cod_vendedor, ArbVendedor->raiz->nombre, ventaTmp );
        }
        CrearListaVendedores(&ArbVendedor->raiz->der, ArbVentas, ListVentas);
        CrearListaVendedores(&ArbVendedor->raiz->izq, ArbVentas, ListVentas);
    }
}

void BorrarArbolVendedor (nom *arb)
{
    if (arb->raiz != NULL)
    {
        BorrarArbol(&arb->raiz->izq);
        BorrarArbol(&arb->raiz->der);
        free (arb->raiz);
        arb->raiz = NULL;

    }
}
void BorrarArbolVentas (vent *arb)
{
    if (arb->raiz != NULL)
    {
        BorrarArbol(&arb->raiz->izq);
        BorrarArbol(&arb->raiz->der);
        free (arb->raiz);
        arb->raiz = NULL;

    }
}
