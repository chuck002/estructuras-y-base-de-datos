#include <stdlib.h>
#include <stdio.h>

#include <string.h>
#include "ListaVendedores.h"

void CrearLista(Entrada *list)
{
    list->raiz = NULL;
}
void AltaNodoLista(Entrada *list, int codigo, char const *nombre, float venta)
{
    Vendedor *nuevo;
    Vendedor *recorre;

    recorre = list->raiz;
    nuevo = malloc(sizeof(Vendedor));

    nuevo->nombre = malloc(strlen(nombre)+1);
    strcpy(nuevo->nombre, nombre);

    nuevo->cod_vendedor = codigo;
    nuevo->venta = venta;
    nuevo->sig = NULL;

    if(list->raiz == NULL)
    {

        list->raiz = nuevo;
        //printf("\ncargado en la raiz -> %i, %s, %f", list->raiz->cod_vendedor, list->raiz->nombre, list->raiz->venta);
    }
    else
    {
        //printf("\nEntro al else");
        while(recorre->sig != NULL)
        {
            recorre = recorre->sig;
        }
        recorre->sig = nuevo;
        //printf("\ncargado al final -> %i, %s, %f", recorre->sig->cod_vendedor, recorre->sig->nombre, recorre->sig->venta);
    }
}
void MostrarListaVendedores(Entrada *list)
{
    Vendedor *recorre;
    recorre = list->raiz;
    if(recorre == NULL)
    {
        printf("\nNo hay nada en la lista.");
    }
    else
    {
        while(recorre != NULL)
        {
            printf("\nCodigo Vendedor: %i", recorre->cod_vendedor);
            printf("\nNombre Vendedor: %s", recorre->nombre);
            printf("\nVenta Maxima: %2f", recorre->venta);
            recorre = recorre->sig;
        }
    }
}

void BorrarLista(Entrada *list)
{
    Vendedor *recorre, *aborrar;
    recorre = list->raiz;
    aborrar = recorre;
    if(list->raiz = NULL)
    {
        printf("\nNo hay nada para borrar.");
    }
    else
    {
        while(recorre != NULL)
    {
        free(aborrar);
        recorre = recorre->sig;
        aborrar = recorre;
    }
            free(list->raiz);
            list->raiz = NULL;
    }
}
