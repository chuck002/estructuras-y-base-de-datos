#ifndef ARBOLBINARIO_H_INCLUDED
#define ARBOLBINARIO_H_INCLUDED

typedef struct Abb
{
    int dato;
    struct Abb *izq, *der;
}abb;

typedef struct arbol{
    abb *raiz;
}Arbol;

void MenuPrincipal();

void CrearArbol(Arbol *);

void AltaDato(Arbol *, int);

void RecorrerPreorden(Arbol *);
void RecorrerInOrden(Arbol *);
void RecorrerPostOrden(Arbol *);

void EstaDato(Arbol *, int);

int CantidadNodos(Arbol *);

int CantidadNiveles(Arbol *);

void BorrarArbol(Arbol *);
int SumaDer(Arbol *, int);

int EstaDatoArbol(Arbol *, int);

void RecorreryBuscar(Arbol *, Arbol *, Arbol *);


#endif // ARBOLBINARIO_H_INCLUDED
