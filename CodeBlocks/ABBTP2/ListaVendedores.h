#ifndef LISTAVENDEDORES_H_INCLUDED
#define LISTAVENDEDORES_H_INCLUDED


typedef struct Lista{
char *nombre;
int cod_vendedor;
float venta;
struct Lista *sig;
}Vendedor;

typedef struct Raiz{
Vendedor *raiz;
}Entrada;

void CrearLista(Entrada *);
void AltaNodoLista(Entrada *, int, char const *, float);
void MostrarListaVendedores(Entrada *);
void BorrarLista(Entrada *);


#endif // LISTAVENDEDORES_H_INCLUDED
