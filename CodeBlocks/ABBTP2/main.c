#include <stdio.h>
#include <stdlib.h>


#include "ArbolBinario.h"
#include "ArbolEmpleados.h"
#include "ListaVendedores.h"


int main()
{
    Arbol arb;
    Arbol a, b, c;
    int opcion = 0;
    int alta = 0;
    int dato = 0;
    char nombre_vendedor[30];
    float venta_vendedor = 0.0;
    nom vendedor;
    vent venta;
    Entrada lista;
    do
    {
        MenuPrincipal();
        do
        {
            printf("\nOpcion: ");
            scanf("%i", &opcion);
            if(opcion < 0 || opcion > 11)
            {
                printf("\nOpcion no valida, debe elegir una opcion del menu.");
            }
        }
        while(opcion < 0 || opcion > 11);
        switch(opcion)
        {
        case 1:
            CrearArbol(&arb);
            printf("\nArbol Creado. ");
            printf("\n");
            break;
        case 2:
            do
            {
                printf("\n(-1 para salir de la carga).\nColoque el valor para el nodo: ");
                scanf("%i", &alta);
                if(alta == -1)
                {
                    break;
                }
                else
                {
                    AltaDato(&arb, alta);
                }
            }
            while(alta != -1);
            printf("\n");
            break;
        case 3:
            printf("\nRecorrer el arbol.");
            RecorrerPreorden(&arb);
            printf("\n");
            RecorrerInOrden(&arb);
            printf("\n");
            RecorrerPostOrden(&arb);
            printf("\n");
            break;
        case 4:

            printf("\nColoque un dato a buscar: ");
            scanf("%i", &dato);
            EstaDato(&arb, dato);
            printf("\n");
            break;
        case 5:
            printf("\nCantidad de nodos: %i.", CantidadNodos(&arb));
            printf("\n");
            break;
        case 6:
            printf("\nCantidad de niveles: %i.", CantidadNiveles(&arb));
            printf("\n");
            break;
        case 7:
            printf("\nBorrando Arbol Binario.");
            BorrarArbol(&arb);
            printf("\n");
            break;
        case 8:
            printf("\nColoca un valor para sumar sus hijos derechos: ");
            scanf("%i", &dato);
            if(SumaDer(&arb, dato) != 0)
            {
                printf("\nEl total de la suma por derecha es: %i.\n", SumaDer(&arb, dato));
            }
            else
            {
                printf("\nEl valor que sigue por derecha es 0.\n");
            }
            break;
        case 9:
            printf("\nCreando arboles A y B...");
            CrearArbol(&a);
            CrearArbol(&b);
            CrearArbol(&c);
            printf("\nArboles creados...\nCargando los datos del Arbol A: ");
            do
            {
                printf("\n(-1 para salir de la carga).\nColoque el valor para el nodo: ");
                scanf("%i", &alta);
                if(alta == -1)
                {
                    break;
                }
                else
                {
                    AltaDato(&a, alta);
                }
            }
            while(alta != -1);
            printf("\n");
            printf("\nCargando los datos del Arbol B: ");
            do
            {
                printf("\n(-1 para salir de la carga).\nColoque el valor para el nodo: ");
                scanf("%i", &alta);
                if(alta == -1)
                {
                    break;
                }
                else
                {
                    AltaDato(&b, alta);
                }
            }
            while(alta != -1);
            printf("\n");
/*
            RecorrerInOrden(&a);
            if(EstaDatoArbol(&a, 2) == 1)
            {
                printf("El dato esta. %i", 2);
            }
            else
            {
                printf("\nNo esta ese ddato.");
            }
*/
            RecorreryBuscar(&a,&b,&c);
            printf("\nEl nuevo arbol de los que estan solo en el Arbol A son: ");
            RecorrerInOrden(&c);
            printf("\nBorrando los arboles...");
            BorrarArbol(&a);
            BorrarArbol(&b);
            BorrarArbol(&c);
            printf("\nBorrados... Adios.");
            break;
        case 10:

            CrearArbolVendedor(&vendedor);
            CrearArbolVenta(&venta);
            CrearLista(&lista);
            printf("\nArboles creados...\nCargando los datos del Arbol VENDEDORES: ");
            do
            {
                printf("\n(-1 para salir de la carga).\nColoque el valor para el Vendedor: ");
                scanf("%i", &alta);
                if(alta == -1)
                {
                    break;
                }
                else
                {
                    printf("\nCarga el nombre del vendedor %i: ", alta);
                    scanf("%s", &nombre_vendedor);
                    AltaDatoVendedor(&vendedor, alta, nombre_vendedor);
                }
            }
            while(alta != -1);
            printf("\n");
            printf("\nCargando los datos del Arbol VENTAS: ");
            do
            {
                printf("\n(-1 para salir de la carga).\nColoque el valor para el Vendedor: ");
                scanf("%i", &alta);
                if(alta == -1)
                {
                    break;
                }
                else
                {
                    printf("\nCarga la venta del vendedor %i: ", alta);
                    scanf("%f", &venta_vendedor);
                    AltaDatoVentas(&venta, alta, venta_vendedor);
                }
            }
            while(alta != -1);
            printf("\n");
            /*
            char prueba[30] = "hola";
            AltaNodoLista(&lista, 3, &prueba, 12.3);
            AltaNodoLista(&lista, 4, "Cecilia", 17.3);
            AltaNodoLista(&lista, 5, "Daniel", 13.3);
            AltaNodoLista(&lista, 8, "Oscar", 11.3);
            printf("\n%s", prueba);
            */
            CrearListaVendedores(&vendedor, &venta, &lista);
            MostrarListaVendedores(&lista);
            BorrarArbol(&vendedor);
            BorrarArbol(&venta);
            BorrarLista(&lista);
            break;
        default:
            break;

        }
    }
    while(opcion != 11);


    return 0;
}
