#ifndef ARBOLEMPLEADOS_H_INCLUDED
#define ARBOLEMPLEADOS_H_INCLUDED

#include "ListaVendedores.h"

typedef struct nombre_vendedor{
    int cod_vendedor;
     char *nombre;
     struct nombre_vendedor *izq, *der;
}nombre_ven;
typedef struct venta_vendedor{
    int cod_vendedor;
    float ventas;
    struct venta_vendedor *izq, *der;
}venta_ven;
typedef struct nom_entrada{
    nombre_ven *raiz;
}nom;
typedef struct vent_entrada{
venta_ven *raiz;
}vent;

void CrearArbolVendedor(nom *);
void CrearArbolVenta(vent *);
void AltaDatoVendedor(nom *, int, char const *);
void AltaDatoVentas(vent *, int, float);
int EstaDatoVendedor(nom *, int);
int EstaDatoVentas(vent *, int);
void CrearListaVendedores(nom *, vent *, Entrada *);
void BorrarArbolVendedor (nom *);
void BorrarArbolVentas (vent *);
float RecibeDatoVenta(vent *, int);

#endif // ARBOLEMPLEADOS_H_INCLUDED
