#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "ListaTDA.h"

// Utilizacion de Pila con Lista Ligada Simple.

int main(int argc, char **argv)
{
    char palabra[50];
    char palabra_reversa[50];
    char *salto;
    pila p;
    Crear(&p);
    printf("Programa de Reconocimiento de Palindromos.\n");
    printf("Por Favor coloque una palabra: ");
    fgets(palabra, 50, stdin);
    salto = strchr(palabra, 10);
    if(salto != NULL)
    {
        *salto = '\0';
    }
    int i = 0;
    do
    {
        palabra[i] = toupper(palabra[i]);
        Apilar(&p, palabra[i]);
        i++;
    }
    while(palabra[i] != '\0');  // Apilamos la palabra
    int j = 0;
    do
    {
        palabra_reversa[j] = Tope(p);
        Desapilar(&p);
        j++;
    }
    while(Vacia(p)!= 1);  // Desapilamos en otro vector para luego compararlos.
    if(strcmp(palabra, palabra_reversa) == 0)
    {
        printf("\nLa palabra (%s) ES UN PALINDROMO. y su reversa es %s", palabra, palabra_reversa);
    }
    else
    {
        printf("\nLa palabra (%s) NO ES UN PALINDROMO, su reversa es %s.", palabra, palabra_reversa);
    }
    Destruir(&p);
    return 0;
}
