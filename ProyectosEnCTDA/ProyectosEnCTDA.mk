##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=ProyectosEnCTDA
ConfigurationName      :=Debug
WorkspaceConfiguration := $(ConfigurationName)
WorkspacePath          :=/home/javy/Documentos/ProyectosEnC
ProjectPath            :=/home/javy/Documentos/ProyectosEnC/ProyectosEnCTDA
IntermediateDirectory  :=../build-$(ConfigurationName)/ProyectosEnCTDA
OutDir                 :=../build-$(ConfigurationName)/ProyectosEnCTDA
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Javier Luna
Date                   :=01/05/20
CodeLitePath           :=/home/javy/.codelite
LinkerName             :=/usr/bin/g++-7
SharedObjectLinkerName :=/usr/bin/g++-7 -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../build-$(ConfigurationName)/bin/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :=$(IntermediateDirectory)/ObjectsList.txt
PCHCompileFlags        :=
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++-7
CC       := /usr/bin/gcc-7
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=../build-$(ConfigurationName)/ProyectosEnCTDA/up_unidad3-tda-lista_lista.c$(ObjectSuffix) ../build-$(ConfigurationName)/ProyectosEnCTDA/up_unidad3-tda-lista_main.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: MakeIntermediateDirs $(OutputFile)

$(OutputFile): ../build-$(ConfigurationName)/ProyectosEnCTDA/.d $(Objects) 
	@mkdir -p "../build-$(ConfigurationName)/ProyectosEnCTDA"
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@mkdir -p "../build-$(ConfigurationName)/ProyectosEnCTDA"
	@mkdir -p ""../build-$(ConfigurationName)/bin""

../build-$(ConfigurationName)/ProyectosEnCTDA/.d:
	@mkdir -p "../build-$(ConfigurationName)/ProyectosEnCTDA"

PreBuild:


##
## Objects
##
../build-$(ConfigurationName)/ProyectosEnCTDA/up_unidad3-tda-lista_lista.c$(ObjectSuffix): ../unidad3-tda-lista/lista.c ../build-$(ConfigurationName)/ProyectosEnCTDA/up_unidad3-tda-lista_lista.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/unidad3-tda-lista/lista.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_unidad3-tda-lista_lista.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/ProyectosEnCTDA/up_unidad3-tda-lista_lista.c$(DependSuffix): ../unidad3-tda-lista/lista.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/ProyectosEnCTDA/up_unidad3-tda-lista_lista.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/ProyectosEnCTDA/up_unidad3-tda-lista_lista.c$(DependSuffix) -MM ../unidad3-tda-lista/lista.c

../build-$(ConfigurationName)/ProyectosEnCTDA/up_unidad3-tda-lista_lista.c$(PreprocessSuffix): ../unidad3-tda-lista/lista.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/ProyectosEnCTDA/up_unidad3-tda-lista_lista.c$(PreprocessSuffix) ../unidad3-tda-lista/lista.c

../build-$(ConfigurationName)/ProyectosEnCTDA/up_unidad3-tda-lista_main.c$(ObjectSuffix): ../unidad3-tda-lista/main.c ../build-$(ConfigurationName)/ProyectosEnCTDA/up_unidad3-tda-lista_main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/unidad3-tda-lista/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_unidad3-tda-lista_main.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/ProyectosEnCTDA/up_unidad3-tda-lista_main.c$(DependSuffix): ../unidad3-tda-lista/main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/ProyectosEnCTDA/up_unidad3-tda-lista_main.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/ProyectosEnCTDA/up_unidad3-tda-lista_main.c$(DependSuffix) -MM ../unidad3-tda-lista/main.c

../build-$(ConfigurationName)/ProyectosEnCTDA/up_unidad3-tda-lista_main.c$(PreprocessSuffix): ../unidad3-tda-lista/main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/ProyectosEnCTDA/up_unidad3-tda-lista_main.c$(PreprocessSuffix) ../unidad3-tda-lista/main.c


-include ../build-$(ConfigurationName)/ProyectosEnCTDA//*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(IntermediateDirectory)


