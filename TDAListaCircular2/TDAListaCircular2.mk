##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=TDAListaCircular2
ConfigurationName      :=Debug
WorkspaceConfiguration := $(ConfigurationName)
WorkspacePath          :=/home/javy/Documentos/ProyectosEnC
ProjectPath            :=/home/javy/Documentos/ProyectosEnC/TDAListaCircular2
IntermediateDirectory  :=../build-$(ConfigurationName)/TDAListaCircular2
OutDir                 :=../build-$(ConfigurationName)/TDAListaCircular2
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Javier Luna
Date                   :=04/05/20
CodeLitePath           :=/home/javy/.codelite
LinkerName             :=/usr/bin/g++-7
SharedObjectLinkerName :=/usr/bin/g++-7 -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../build-$(ConfigurationName)/bin/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :=$(IntermediateDirectory)/ObjectsList.txt
PCHCompileFlags        :=
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++-7
CC       := /usr/bin/gcc-7
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=../build-$(ConfigurationName)/TDAListaCircular2/main.c$(ObjectSuffix) ../build-$(ConfigurationName)/TDAListaCircular2/ListaTDA.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: MakeIntermediateDirs $(OutputFile)

$(OutputFile): ../build-$(ConfigurationName)/TDAListaCircular2/.d $(Objects) 
	@mkdir -p "../build-$(ConfigurationName)/TDAListaCircular2"
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@mkdir -p "../build-$(ConfigurationName)/TDAListaCircular2"
	@mkdir -p ""../build-$(ConfigurationName)/bin""

../build-$(ConfigurationName)/TDAListaCircular2/.d:
	@mkdir -p "../build-$(ConfigurationName)/TDAListaCircular2"

PreBuild:


##
## Objects
##
../build-$(ConfigurationName)/TDAListaCircular2/main.c$(ObjectSuffix): main.c ../build-$(ConfigurationName)/TDAListaCircular2/main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/TDAListaCircular2/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/TDAListaCircular2/main.c$(DependSuffix): main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/TDAListaCircular2/main.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/TDAListaCircular2/main.c$(DependSuffix) -MM main.c

../build-$(ConfigurationName)/TDAListaCircular2/main.c$(PreprocessSuffix): main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/TDAListaCircular2/main.c$(PreprocessSuffix) main.c

../build-$(ConfigurationName)/TDAListaCircular2/ListaTDA.c$(ObjectSuffix): ListaTDA.c ../build-$(ConfigurationName)/TDAListaCircular2/ListaTDA.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/TDAListaCircular2/ListaTDA.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/ListaTDA.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/TDAListaCircular2/ListaTDA.c$(DependSuffix): ListaTDA.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/TDAListaCircular2/ListaTDA.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/TDAListaCircular2/ListaTDA.c$(DependSuffix) -MM ListaTDA.c

../build-$(ConfigurationName)/TDAListaCircular2/ListaTDA.c$(PreprocessSuffix): ListaTDA.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/TDAListaCircular2/ListaTDA.c$(PreprocessSuffix) ListaTDA.c


-include ../build-$(ConfigurationName)/TDAListaCircular2//*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(IntermediateDirectory)


