#include <stdio.h>
#include "ListaTDA.h"

int main(int argc, char **argv)
{
	lista l;
	MostrarMenu();
	Crear(&l);
	AltaFrente(&l, 3);
	AltaFrente(&l, 7);
	AltaFrente(&l, 9);
	AltaFinal(&l, 40);
	AltaFinal(&l, 50);
	Recorre(l);
	Destruir(&l);
	return 0;
}