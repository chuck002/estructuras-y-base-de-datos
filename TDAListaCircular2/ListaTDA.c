
#include <stdio.h>
#include <stdlib.h>

#include "ListaTDA.h"

nodo *ultimo;

void Crear(lista *l)
{
	l->lis = NULL;
	ultimo = NULL;
}

void Destruir(lista *l)
{
	if(l->lis == NULL)
	{
		printf("\noNo hay nada para destruir.");
	}else{
	nodo *p = l->lis;
	nodo *proximo;
	do
	{
		proximo = p->sig;
		free(p);
		p = proximo;
	}while(p != NULL);
	if(p == NULL)
	{
	printf("\n\n\nHa destruido la lista.");
	}
	}
}

void AltaFrente(lista *l, int valor)
{
	nodo *nuevo = (nodo*)malloc(sizeof(nodo));
	nuevo->valor = valor;
	if(l->lis == NULL)
	{
		l->lis = nuevo;
		ultimo = nuevo;
		l->lis->sig = l->lis;
		ultimo->sig = l->lis;
	}
	else
	{
		nuevo->sig = l->lis;
		l->lis = nuevo;
	}
}

void AltaFinal(lista *l, int valor)
{
	nodo *nuevo = (nodo*)malloc(sizeof(nodo));
	nuevo->valor = valor;
	nuevo->sig = l->lis;
	if(l->lis == NULL)
	{
		l->lis = nuevo;
		ultimo = nuevo;
		ultimo->sig = l->lis;
	}else{
		ultimo->sig = nuevo;
		ultimo = nuevo;
	}
}
int vacia(lista l)
{
	if(l.lis == NULL)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int Buscar(lista l, int valor)
{
	if(l.lis == NULL)
	{
		printf("\nNo hay datos para mostrar en la lista.");
	}else{
		nodo * tmp = l.lis;
		do
		{
			if(tmp->valor == valor)
			{
				return tmp->valor;
			}
			tmp = tmp->sig;
		}while(tmp != l.lis);
	}
	return 0;
}

void Recorre(lista l)
{
	if(l.lis == NULL)
	{
		printf("\nNo hay datos para mostrar.");
	}else
	{
	nodo *recorre = l.lis;
	do
	{
		printf("\nValor: %d", recorre->valor);
		recorre = recorre->sig;
	}while(recorre != ultimo);
	printf("\n");
	}
}

void Borrar(lista *l, int valor)
{
	if(l->lis == NULL)
	{
		printf("\nNo hay nada para borrar.");
	}else{
		nodo *borra = l->lis;
		do
		{
			borra = l->lis;
			l->lis = l->lis->sig;
			free(borra);
		}while(l->lis != NULL);
	}
}

void MostrarMenu()
{
	printf("\nBienvenido al programa de listas Circulares.");
	printf("\nPor favor elija una opcion. ");
	printf("\n\n1. Crear Lista.");
	printf("\n2. Borrar Lista.");
	printf("\n3. Agregar dato al principio.");
	printf("\n4. Agregar dato al final.");
	printf("\n5. Borrar dato del principio.");
	printf("\n6. Borrar dato del final.");
	printf("\n7. Mostrar todos los elementos.");
	printf("\n8. Buscar un elemento.");
	printf("\n9. Mostrar la cantidad de elementos de la lista.");
	printf("\n\nElige una opcion: ");
}