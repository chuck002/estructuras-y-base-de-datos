#ifndef LISTATDA_H_INCLUDED
#define LISTATDA_H_INCLUDED

typedef struct Nodo{
	struct Nodo* sig;
	int valor;
}nodo;

typedef struct Lista{
	nodo* lis; 
}lista;


void Crear(lista *);

void Destruir(lista *);

void AltaFrente(lista *, int);

void AltaFinal(lista *, int);

int Buscar(lista, int);

void Recorre(lista);

void Borrar(lista *, int);

void MostrarMenu();

#endif // LISTATDA_H_INCLUDED