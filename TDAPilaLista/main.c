#include <stdio.h>

#include "TDAPilaLista.h"

int main(int argc, char **argv)
{
	pila p;
	Crear(&p);
	Apilar(&p, 2);
	Apilar(&p, 7);
	Apilar(&p, 10);
	Apilar(&p, 20);
	Apilar(&p, 3);
	while(p.p != NULL)
	{
			printf("%i ", Tope(p));
			Desapilar(&p);
	}
	Destruir(&p);
}
