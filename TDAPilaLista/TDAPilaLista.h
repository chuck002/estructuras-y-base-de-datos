#ifndef LISTATDAPILALISTA_H_INCLUDED
#define LISTATDAPILALISTA_H_INCLUDED

typedef struct Nodo{
	int valor;
	struct Nodo *sig;
}nodo;

typedef struct Pila
{
	nodo *p;
}pila;

void Crear(pila*);

void Destruir(pila*);

void Apilar(pila*, int);

void Desapilar(pila*);

int Tope(pila);

int Vacia(pila);

#endif // LISTATDAPILALISTA_H_INCLUDED