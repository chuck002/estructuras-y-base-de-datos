##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=TDAPilaLista
ConfigurationName      :=Debug
WorkspaceConfiguration := $(ConfigurationName)
WorkspacePath          :=/home/javy/Documentos/ProyectosEnC
ProjectPath            :=/home/javy/Documentos/ProyectosEnC/TDAPilaLista
IntermediateDirectory  :=../build-$(ConfigurationName)/TDAPilaLista
OutDir                 :=../build-$(ConfigurationName)/TDAPilaLista
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Javier Luna
Date                   :=04/05/20
CodeLitePath           :=/home/javy/.codelite
LinkerName             :=/usr/bin/g++-7
SharedObjectLinkerName :=/usr/bin/g++-7 -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../build-$(ConfigurationName)/bin/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :=$(IntermediateDirectory)/ObjectsList.txt
PCHCompileFlags        :=
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++-7
CC       := /usr/bin/gcc-7
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=../build-$(ConfigurationName)/TDAPilaLista/main.c$(ObjectSuffix) ../build-$(ConfigurationName)/TDAPilaLista/TDAPilaLista.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: MakeIntermediateDirs $(OutputFile)

$(OutputFile): ../build-$(ConfigurationName)/TDAPilaLista/.d $(Objects) 
	@mkdir -p "../build-$(ConfigurationName)/TDAPilaLista"
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@mkdir -p "../build-$(ConfigurationName)/TDAPilaLista"
	@mkdir -p ""../build-$(ConfigurationName)/bin""

../build-$(ConfigurationName)/TDAPilaLista/.d:
	@mkdir -p "../build-$(ConfigurationName)/TDAPilaLista"

PreBuild:


##
## Objects
##
../build-$(ConfigurationName)/TDAPilaLista/main.c$(ObjectSuffix): main.c ../build-$(ConfigurationName)/TDAPilaLista/main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/TDAPilaLista/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/TDAPilaLista/main.c$(DependSuffix): main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/TDAPilaLista/main.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/TDAPilaLista/main.c$(DependSuffix) -MM main.c

../build-$(ConfigurationName)/TDAPilaLista/main.c$(PreprocessSuffix): main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/TDAPilaLista/main.c$(PreprocessSuffix) main.c

../build-$(ConfigurationName)/TDAPilaLista/TDAPilaLista.c$(ObjectSuffix): TDAPilaLista.c ../build-$(ConfigurationName)/TDAPilaLista/TDAPilaLista.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/TDAPilaLista/TDAPilaLista.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/TDAPilaLista.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/TDAPilaLista/TDAPilaLista.c$(DependSuffix): TDAPilaLista.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/TDAPilaLista/TDAPilaLista.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/TDAPilaLista/TDAPilaLista.c$(DependSuffix) -MM TDAPilaLista.c

../build-$(ConfigurationName)/TDAPilaLista/TDAPilaLista.c$(PreprocessSuffix): TDAPilaLista.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/TDAPilaLista/TDAPilaLista.c$(PreprocessSuffix) TDAPilaLista.c


-include ../build-$(ConfigurationName)/TDAPilaLista//*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(IntermediateDirectory)


