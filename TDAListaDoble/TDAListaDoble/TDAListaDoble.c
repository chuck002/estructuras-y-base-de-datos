#include <stdio.h>
#include <stdlib.h>


#include "TDAListaDoble.h"

void Crear(lista *l)
{
	l->lis = malloc(sizeof(nodo));
	//l->lis = NULL;
	l->lis->sig = NULL;
	l->lis->ant = NULL;
	/*nodo *nuevo = (nodo*)malloc(sizeof(nodo));
	nuevo->valor = 5;
	if(nuevo == NULL)
	{
		printf("\nNo se ha creado el espacio.");
	}
	else{
		printf("\n\n Se ha conseguido el espacio.");
	}
	lis->lis = NULL;*/
	//printf("\n\nVALOR DE MEMORIA %p", l->lis);
	//printf("\nVALOR: %d", l->lis->valor);
}

void Destruir(lista *l)
{
	if(l->lis == NULL)
	{
		printf("\nLa lista esta vacia.\n\n");
	}
	else{
	printf("\nLa lista esta llena... pero no se ha podido vaciar... ");
	nodo *p = l->lis;
	nodo *proximo;
	do
	{
		proximo = p->sig;
		free(p);
		p = proximo;
	}while(p != NULL);
	if(p == NULL)
	{
	printf("\n\n\nHa destruido la lista.");
	}
	}
}
void AltaFrente(lista *l, int valor)
{
	nodo *nuevo = (nodo*)malloc(sizeof(nodo));
	nuevo->valor = valor;
	nuevo->sig = l->lis;
	nuevo->ant = NULL;
	if(l->lis != NULL)
	{
	if(l->lis->ant == NULL && l->lis->sig == NULL)
	{
		l->lis = nuevo;
	}
	else{
		l->lis->ant = nuevo;
		l->lis = nuevo;
	}
	}
	
}

void AltaFinal(lista *l, int valor)
{
	nodo *nuevo = (nodo*)malloc(sizeof(nodo));
	nuevo->valor = valor;
	nuevo->sig = NULL;
	nodo *ultimo = l->lis;
	while(ultimo->sig != NULL)
	{
	ultimo = ultimo->sig;
	}
	ultimo->sig = nuevo;
	
	nuevo->ant = ultimo;
	ultimo = nuevo;
}

int Buscar(lista l, int valor)
{
	nodo *tmp = l.lis;
	while(tmp != NULL)
	{
		if(valor == tmp->valor)
		{
			return valor;
		}
		tmp = tmp->sig;
	}
	return 0;
}

void Recorre(lista l)
{
	nodo *recorre = l.lis;
	//printf("\n%d, %d, %d, %d, %d", recorre->valor, recorre->sig->valor, recorre->sig->sig->valor,
	//recorre->sig->sig->sig->valor, recorre->sig->sig->sig->sig->valor);
	do
	{
		printf("\nValor: %d", recorre->valor);
		recorre = recorre->sig;
	}while(recorre != NULL);
}

void Borrar(lista *l, int valor)
{

	nodo *tmp = l->lis;
	while(tmp != NULL)
	{
		if(valor == tmp->valor)
		{
		nodo *tmpAnt = tmp->ant;
		nodo *tmpSig = tmp->sig;
		tmpAnt->sig = tmpSig;
		tmpSig->ant = tmpAnt;
		free(tmp);
		}
		tmp = tmp->sig;
	}
}
