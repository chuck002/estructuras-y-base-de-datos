##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=TDAListaDoble
ConfigurationName      :=Debug
WorkspaceConfiguration := $(ConfigurationName)
WorkspacePath          :=/home/javy/Documentos/ProyectosEnC
ProjectPath            :=/home/javy/Documentos/ProyectosEnC/TDAListaDoble/TDAListaDoble
IntermediateDirectory  :=../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble
OutDir                 :=../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Javier Luna
Date                   :=27/05/20
CodeLitePath           :=/home/javy/.codelite
LinkerName             :=/usr/bin/g++-7
SharedObjectLinkerName :=/usr/bin/g++-7 -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../../build-$(ConfigurationName)/bin/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :=$(IntermediateDirectory)/ObjectsList.txt
PCHCompileFlags        :=
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++-7
CC       := /usr/bin/gcc-7
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/main.c$(ObjectSuffix) ../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/TDAListaDoble.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: MakeIntermediateDirs $(OutputFile)

$(OutputFile): ../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/.d $(Objects) 
	@mkdir -p "../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble"
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@mkdir -p "../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble"
	@mkdir -p ""../../build-$(ConfigurationName)/bin""

../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/.d:
	@mkdir -p "../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble"

PreBuild:


##
## Objects
##
../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/main.c$(ObjectSuffix): main.c ../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/TDAListaDoble/TDAListaDoble/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IncludePath)
../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/main.c$(DependSuffix): main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/main.c$(ObjectSuffix) -MF../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/main.c$(DependSuffix) -MM main.c

../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/main.c$(PreprocessSuffix): main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/main.c$(PreprocessSuffix) main.c

../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/TDAListaDoble.c$(ObjectSuffix): TDAListaDoble.c ../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/TDAListaDoble.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/javy/Documentos/ProyectosEnC/TDAListaDoble/TDAListaDoble/TDAListaDoble.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/TDAListaDoble.c$(ObjectSuffix) $(IncludePath)
../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/TDAListaDoble.c$(DependSuffix): TDAListaDoble.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/TDAListaDoble.c$(ObjectSuffix) -MF../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/TDAListaDoble.c$(DependSuffix) -MM TDAListaDoble.c

../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/TDAListaDoble.c$(PreprocessSuffix): TDAListaDoble.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble/TDAListaDoble.c$(PreprocessSuffix) TDAListaDoble.c


-include ../../build-$(ConfigurationName)/TDAListaDoble/TDAListaDoble//*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(IntermediateDirectory)


