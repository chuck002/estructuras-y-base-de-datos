#include <stdio.h>

#include "TDAListaDoble.h"

int main(int argc, char **argv)
{
	lista l;
	Crear(&l);
	AltaFrente(&l, 3);
	AltaFrente(&l, 7);
	AltaFrente(&l, 9);
	AltaFrente(&l, 22);
	AltaFinal(&l, 27);
	AltaFinal(&l, 20);
	AltaFinal(&l, 24);
	AltaFinal(&l, 30);
	Recorre(l);
	printf("\nBuscamos el valor 21, si esta en la lista lo devuelve, sino devuelve cero.\nResultado: %d\n", Buscar(l, 21));
	printf("\nBuscamos el valor 20, si esta en la lista lo devuelve, sino devuelve cero.\nResultado: %d\n", Buscar(l, 20));
	Borrar(&l, 20);
	Recorre(l);
		printf("Buscamos el valor 20, si esta en la lista lo devuelve, sino devuelve cero.\nResultado: %d\n", Buscar(l, 20));
	Destruir(&l);
	return 0;
}
